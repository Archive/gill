/*
 * An Embeddable Bonobo component wrapper for Gill.
 *
 * This file contains the implementation of the EmbeddableGill object.
 * EmbeddableGill inherits from BonoboEmbeddable and can be used to
 * embed editable in-place Gill documents in applications.  It also
 * includes an implementation of the EmbeddableGill factory object
 * which is used to instantiate EmbeddableGill components.
 *
 * Copyright 1999, International GNOME Support.
 *
 * Author:
 *   Nat Friedman (nat@helixcode.com)
 */

#include <config.h>
#include <bonobo.h>
#include "embeddable-gill.h"
#include "svg.h"
#include "svg-util.h"

/*
 * Number of running objects
 */
static int running_objects = 0;

/*
 * Some prototypes.
 */
static int ps_save (BonoboPersistStream *ps, Bonobo_Stream stream, void *closure);
static int ps_load (BonoboPersistStream *ps, Bonobo_Stream stream, void *closure);
static int pf_save (BonoboPersistFile *pf, const CORBA_char *filename, void *closure);
static int pf_load (BonoboPersistFile *pf, const CORBA_char *filename, void *closure);
static void gill_view_setup (GillView *view);
static BonoboCanvasComponent *
embeddable_gill_item_factory (BonoboEmbeddable *embeddable, GnomeCanvas *canvas, void *user_data);

static BonoboEmbeddableFactory *bonobo_embeddable_gill_factory;

/*
 * The EmbeddableGill object implementation.
 */

static void
embeddable_gill_class_init (GtkObjectClass *class)
{
}

static void
embeddable_gill_init (GtkObject *object)
{
}

static BonoboView *
embeddable_gill_view_factory (BonoboEmbeddable *embeddable,
			      const Bonobo_ViewFrame view_frame,
			      void *data)
{
	BonoboView *view;

	view = gill_view_new (EMBEDDABLE_GILL (embeddable));

	return BONOBO_VIEW (view);
}

EmbeddableGill *
embeddable_gill_new (void)
{
	EmbeddableGill *gill;
	Bonobo_Embeddable corba_gill;
	BonoboPersistFile *pf;
	BonoboPersistStream *ps;

	/*
	 * Construct the object.
	 */
	gill = gtk_type_new (EMBEDDABLE_GILL_TYPE);

	corba_gill = bonobo_embeddable_corba_object_create (BONOBO_OBJECT (gill));
	if (corba_gill == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (gill));
		return NULL;
	}

	bonobo_embeddable_construct_full (
		BONOBO_EMBEDDABLE (gill), corba_gill,
		embeddable_gill_view_factory, NULL,
		embeddable_gill_item_factory, NULL);

	/*
	 * Add the PersistFile interface.
	 */
	pf = bonobo_persist_file_new (pf_load, pf_save,
				     gill);

	if (pf == NULL) {
		gtk_object_unref (GTK_OBJECT (gill));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (gill), BONOBO_OBJECT (pf));

	/*
	 * Add the PersistStream interface.
	 */
	ps = bonobo_persist_stream_new (ps_load, ps_save, gill);

	if (ps == NULL) {
		gtk_object_unref (GTK_OBJECT (gill));
		gtk_object_unref (GTK_OBJECT (pf));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (gill), BONOBO_OBJECT (ps));	

	return gill;
}

GtkType
embeddable_gill_get_type (void)
{
	static GtkType type = 0;

	if (! type) {
		GtkTypeInfo info = {
			"EmbeddableGill",
			sizeof (EmbeddableGill),
			sizeof (EmbeddableGillClass),
			(GtkClassInitFunc) embeddable_gill_class_init,
			(GtkObjectInitFunc) embeddable_gill_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};
		type = gtk_type_unique (bonobo_embeddable_get_type (), &info);
	}
	
	return type;
}

static void
do_gill_setup_view (BonoboView *view, void *data)
{
	gill_view_setup (GILL_VIEW (view));
}

static void
embeddable_gill_setup_views (EmbeddableGill *gill)
{
	bonobo_embeddable_foreach_view (BONOBO_EMBEDDABLE (gill), do_gill_setup_view, NULL);
}

static void
do_gill_clear_view (BonoboView *bonobo_view, void *data)
{
	GillView *view = GILL_VIEW (bonobo_view);
	
	if (view->canvas != NULL) {
		gtk_widget_destroy (view->canvas);
		view->canvas = NULL;
	}
}

static void
embeddable_gill_clear_views (EmbeddableGill *gill)
{
	bonobo_embeddable_foreach_view (BONOBO_EMBEDDABLE (gill), do_gill_clear_view, NULL);
}

static void
embeddable_gill_unload_data (EmbeddableGill *gill)
{
	/*
	 * Free the internal document data.
	 */
	if (gill->gdome_doc != NULL) {
		GdomeException exc = 0;

		gdome_doc_unref (gill->gdome_doc, &exc);
		gill->gdome_doc = NULL;
		gill->gdome_el = NULL;
	}

	if (gill->xml_doc != NULL) {
		xmlFreeDoc (gill->xml_doc);
		gill->xml_doc = NULL;
	}

	/*
	 * Clear all the views.
	 */
	embeddable_gill_clear_views (gill);
}

static int
embeddable_gill_create_dom (EmbeddableGill *gill)
{
	GdomeException exc = 0;
	GdomeElement *element;

	gill->gdome_doc = gdome_xml_from_document (gill->xml_doc);

	if (gill->gdome_doc == NULL)
		return -1;

	element = gdome_doc_documentElement (gill->gdome_doc, &exc);
	gill->gdome_el = element;
	
	if (exc != 0) {
		GdomeException exc_tmp = 0;

		gdome_doc_unref (gill->gdome_doc, &exc_tmp);

		gill->gdome_doc = NULL;
		gill->gdome_el = NULL;

		return -1;
	}

	return 0;
}


/*
 * The PersistFile interface implementation for an EmbeddableGill
 * component.
 */
static int
pf_load (BonoboPersistFile *pf, const CORBA_char *filename, void *closure)
{
	EmbeddableGill *gill = EMBEDDABLE_GILL (closure);

	/*
	 * Unload any existing data.
	 */
	embeddable_gill_unload_data (gill);

	/*
	 * Load the new file.
	 */
	gill->xml_doc = xmlParseFile (filename);

	if (gill->xml_doc == NULL)
		return -1;

	if (embeddable_gill_create_dom (gill) < 0) {
		xmlFreeDoc (gill->xml_doc);
		gill->xml_doc = NULL;
		return -1;
	}

	/*
	 * Tell all the views to display the new document.
	 */
	embeddable_gill_setup_views (gill);

	return 0;
}

static int
pf_save (BonoboPersistFile *pf, const CORBA_char *filename, void *closure)
{
	g_warning ("embeddable-gill: PersistFile_Save unimplemented!\n");
	return 0;
}



/*
 * The PersistStream interface implementation for an EmbeddableGill
 * component.
 */
static int
stream_read (Bonobo_Stream stream,
	     char **buffer, size_t *sz)
{
	Bonobo_Stream_iobuf *iobuf;
	CORBA_long bytes_read;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	
	*buffer = NULL;
	*sz = 0;

	do {
#define READ_CHUNK_SIZE 65536
		bytes_read = Bonobo_Stream_read (stream, READ_CHUNK_SIZE,
						&iobuf, &ev);

		*buffer = g_realloc (*buffer, *sz + iobuf->_length);
		memcpy (*buffer + *sz, iobuf->_buffer, iobuf->_length);

		*sz += iobuf->_length;
					
	} while (bytes_read > 0);

	CORBA_exception_free (&ev);
	
	if (bytes_read < 0)
		return -1;

	return 0;
}

static int
ps_load (BonoboPersistStream *ps, Bonobo_Stream stream, void *closure)
{
	EmbeddableGill *gill = EMBEDDABLE_GILL (closure);
	char *buffer;
	size_t sz;

	/*
	 * Unload any existing document.
	 */
	embeddable_gill_unload_data (gill);

	/*
	 * Load the stream into memory.
	 */
	stream_read (stream, &buffer, &sz);

	gill->xml_doc = xmlParseMemory (buffer, (int) sz);

	g_free (buffer);

	if (gill->xml_doc == NULL)
		return -1;

	if (embeddable_gill_create_dom (gill) < 0)
		return -1;

	/*
	 * Tell all the views to display the new document.
	 */
	embeddable_gill_setup_views (gill);

	return 0;
}

static int
ps_save (BonoboPersistStream *ps, Bonobo_Stream stream, void *closure)
{
	g_warning ("gill-bonobo: PersistStream_save unimplemented!\n");
	return 0;
}



/*
 * This method gets invoked when an EmbeddableGill is destroyed.
 * If we reach the zero count for running objects, we shut down
 */
static void
embeddable_object_destroyed (EmbeddableGill *gill)
{
	running_objects--;

	if (running_objects != 0)
		return;

	/*
	 * Last object is gone, unref the factory and quit
	 */
	bonobo_object_unref (BONOBO_OBJECT (bonobo_embeddable_gill_factory));
	gtk_main_quit ();
}

/*
 * The EmbeddableGill factory.
 */
static BonoboObject *
embeddable_gill_factory (BonoboEmbeddableFactory *this, void *data)
{
	EmbeddableGill *embeddable_gill;

	embeddable_gill = embeddable_gill_new ();

	if (embeddable_gill == NULL)
		return NULL;

	running_objects++;
	gtk_signal_connect (
		GTK_OBJECT (embeddable_gill), "destroy",
		GTK_SIGNAL_FUNC (embeddable_object_destroyed), NULL);
	
	return BONOBO_OBJECT (embeddable_gill);
}

void
EmbeddableGillFactory_init (void)
{
	if (bonobo_embeddable_gill_factory != NULL)
		return;

	/*
	 * Gill internally uses GdkRGB
	 */
	gdk_rgb_init ();
	
	bonobo_embeddable_gill_factory = bonobo_embeddable_factory_new (
		"embeddable-factory:gill", embeddable_gill_factory, NULL);

	if (bonobo_embeddable_gill_factory == NULL)
		g_error ("It was not possible to register the new Gill object factory");
}


/*
 * GillView implementation.
 */
GtkType
gill_view_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"EmbeddableGillView",
			sizeof (GillView),
			sizeof (GillViewClass),
			NULL, NULL, NULL, NULL, NULL
		};

		type = gtk_type_unique (bonobo_view_get_type (), &info);
	}
	
	return type;
}

static void
gill_view_create_menus (GillView *view)
{
}

static void
gill_view_destroy_menus (GillView *view)
{
}

static void
gill_view_activate_cb (BonoboView *view,
		       gboolean activate,
		       gpointer data)
{
	/*
	 * If we have been activated, create our menus.  If we have
	 * been deactivated, destroy them.
	 */
	if (activate)
		gill_view_create_menus (GILL_VIEW (view));
	else
		gill_view_destroy_menus (GILL_VIEW (view));

	/*
	 * Notify the ViewFrame that we accept to be activated or
	 * deactivated (we are an acquiescent BonoboView, yes we are).
	 */
	bonobo_view_activate_notify (view, activate);

	return;	
}

static GtkWidget *
gill_view_create_canvas (GillView *gill_view, GdomeNode *node)
{
	GtkWidget *canvas;
	GnomeCanvasGroup *root;
	RenderContext *rc;
	RenderObj *root_ro;
	double x1, y1, x2, y2;

	canvas = gnome_canvas_new_aa ();

	root = gnome_canvas_root (GNOME_CANVAS (canvas));

	rc = svg_render_context_new ();
	root_ro = domination_render (rc, root, node);

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (root), &x1, &y1, &x2, &y2);
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), x1, y1, x2, y2);

	gill_view->width = ABS (x2 - x1);
	gill_view->height = ABS (y2 - y1);

	gtk_widget_set_usize (canvas, gill_view->width, gill_view->height);

	return canvas;
}

static void
gill_view_setup (GillView *view)
{
	if (view->canvas != NULL) {
		gtk_widget_destroy (view->canvas);
		view->canvas = NULL;
	}
	
	gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	gtk_widget_set_default_visual (gdk_rgb_get_visual ());
	
	view->canvas = gill_view_create_canvas (view, (GdomeNode *)view->embeddable->gdome_el);
	
	gtk_box_pack_start (GTK_BOX (view->toplevel), view->canvas, TRUE, TRUE, 0);
	
	gtk_widget_show_all (view->toplevel);
}

static void
render_when_displayed (GtkWidget *w, GillView *gill_view)
{
	/*
	 * If there is a document loaded into the EmbeddableGill,
	 * render the document's data into a canvas.
	 */
	if (gill_view->embeddable->gdome_el != NULL)
		gill_view_setup (gill_view);
}

/* dead code ?*/
#warning is item_set_bounds dead code ? if so, please, remove it.
/*
static void
item_set_bounds (BonoboCanvasComponent *component, Bonobo_Canvas_DRect *bbox,
		 CORBA_Environment *ev, BonoboEmbeddable *embeddable)
{
	GnomeCanvasItem *item;
	double x0, y0, x1, y1;
	double affine [6], scale [6], translate [6];
	double x_fact, y_fact;
	
	item = bonobo_canvas_component_get_item (component);
	
	gnome_canvas_item_get_bounds (item, &x0, &y0, &x1, &y1);
	art_affine_translate (translate, bbox->x0 - x0, bbox->y0 - y0);
	if (x1 == x0)
		x_fact = 1.0;
	else
		x_fact = x1 - x0;

	if (y1 == y0)
		y_fact = 1.0;
	else
		y_fact = x1 - x0;
	
	art_affine_scale (scale,
			  (bbox->x1 - bbox->x0) / x_fact,
			  (bbox->y1 - bbox->y0) / y_fact);

	art_affine_multiply (affine, translate, scale);
	gnome_canvas_item_affine_relative (item, affine);
}
*/

static BonoboCanvasComponent *
embeddable_gill_item_factory (BonoboEmbeddable *bonobo_embeddable, GnomeCanvas *canvas, void *user_data)
{
	GnomeCanvasItem *root;
	EmbeddableGill *embeddable = EMBEDDABLE_GILL (bonobo_embeddable);
	BonoboCanvasComponent *component;
	
	root = gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (canvas)),
		gnome_canvas_group_get_type (),
		"x", 0.0,
		"y", 0.0,
		NULL);
	
	if (embeddable->gdome_el){
		RenderContext *rc;
		RenderObj *root_ro;

		rc = svg_render_context_new ();
		root_ro = domination_render (rc, GNOME_CANVAS_GROUP(root), (GdomeNode *)embeddable->gdome_el);
	}

	component = bonobo_canvas_component_new (root);

	return component;
}

BonoboView *
gill_view_new (EmbeddableGill *eg)
{
	GillView *gill_view;
	Bonobo_View corba_gill_view;

	/*
	 * Create the GillView.
	 */
	gill_view = gtk_type_new (GILL_VIEW_TYPE);

	corba_gill_view = bonobo_view_corba_object_create (BONOBO_OBJECT (gill_view));
	if (corba_gill_view == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (corba_gill_view));
		return NULL;
	}

	gill_view->embeddable = eg;

	/*
	 * Create its widgets.
	 */
	gill_view->toplevel = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (gill_view->toplevel);

	bonobo_view_construct (BONOBO_VIEW (gill_view), corba_gill_view, GTK_WIDGET (gill_view->toplevel));
	gtk_signal_connect (GTK_OBJECT (gill_view->toplevel), "realize",
			    render_when_displayed, gill_view);
			    
	/*
	 * Create a UIHandler so that we can merge menu and toolbar
	 * items when we are activated.
	 */
	gill_view->uih = bonobo_ui_handler_new ();

	/*
	 * Connect to the activation signal so that we can be
	 * activated in-place.
	 */
	gtk_signal_connect (GTK_OBJECT (gill_view), "activate",
			    GTK_SIGNAL_FUNC (gill_view_activate_cb), NULL);


	return BONOBO_VIEW (gill_view);
}

/*
 * GillCanvasItem type
 */
static BonoboCanvasComponentClass *gill_canvas_item_parent_class;

static void
gci_set_bounds (BonoboCanvasComponent *comp, Bonobo_Canvas_DRect *bbox, CORBA_Environment *ev)
{
	
}

static void
gill_canvas_item_class_init (GtkObjectClass *object_class)
{
	BonoboCanvasComponentClass *cc_class = (BonoboCanvasComponentClass *) object_class;
	
	gill_canvas_item_parent_class = gtk_type_class (bonobo_canvas_component_get_type ());

	cc_class->set_bounds = gci_set_bounds;
}

GtkType
gill_canvas_item_get_type (void)
{
	static GtkType type = 0;

	if (! type) {
		GtkTypeInfo info = {
			"GillCanvasItem",
			sizeof (GillCanvasItem),
			sizeof (GillCanvasItemClass),
			(GtkClassInitFunc) gill_canvas_item_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};
		type = gtk_type_unique (gnome_canvas_item_get_type (), &info);
	}
	
	return type;
}

BonoboCanvasComponent *
gill_canvas_item_new (EmbeddableGill *embeddable_gill, GnomeCanvasItem *item)
{
	BonoboCanvasComponent *comp;

	comp = gtk_type_new (gill_canvas_item_get_type ());

/*	bonobo_canvas_component_construct (comp, item);
	GILL_CANVAS_ITEM (comp)->embeddable_gill = embeddable_gill;
*/
	g_error ("Not finished");
	return NULL;
}
