#ifndef EMBEDDABLE_GILL_H
#define EMBEDDABLE_GILL_H

#include <bonobo/bonobo-embeddable.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/tree.h>
#include <libgdome/gdome.h>
#include <libgdome/gdome-xml.h>


#define EMBEDDABLE_GILL_TYPE	(embeddable_gill_get_type ())
#define EMBEDDABLE_GILL(o)          (GTK_CHECK_CAST ((o), EMBEDDABLE_GILL_TYPE, EmbeddableGill))
#define EMBEDDABLE_GILL_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), EMBEDDABLE_GILL_TYPE, EmbeddableGillClass))
#define IS_EMBEDDABLE_GILL(o)       (GTK_CHECK_TYPE ((o), EMBEDDABLE_GILL_TYPE))
#define IS_EMBEDDABLE_GILL_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), EMBEDDABLE_GILL_TYPE))

/*
 * The EmbeddableGill object.
 *
 * We derive a new object from BonoboEmbeddable to be used to embed
 * Gill into applications.
 */
struct _EmbeddableGill;
typedef struct _EmbeddableGill EmbeddableGill;

struct _EmbeddableGill {
	/* The parent object. */
	BonoboEmbeddable  embeddable;

	/*
	 * The document-specific data.
	 */
	xmlDocPtr	 xml_doc;
	GdomeDocument	*gdome_doc;
	GdomeElement	*gdome_el;
};

typedef struct {
	BonoboEmbeddableClass parent_class;
} EmbeddableGillClass;

GtkType         embeddable_gill_get_type     (void);
EmbeddableGill *embeddable_gill_new          (void);
void            EmbeddableGillFactory_init   (void);

/*
 * The GillView object.
 *
 * This is an extension of the BonoboView object.
 */
#define GILL_VIEW_TYPE        (gill_view_get_type ())
#define GILL_VIEW(o)          (GTK_CHECK_CAST ((o), GILL_VIEW_TYPE, GillView))
#define GILL_VIEW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GILL_VIEW_TYPE, GillViewClass))
#define IS_GILL_VIEW(o)       (GTK_CHECK_TYPE ((o), GILL_VIEW_TYPE))
#define IS_GILL_VIEW_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GILL_VIEW_TYPE))

typedef struct _GillView GillView;

struct _GillView {
	BonoboView	 view;

	EmbeddableGill	*embeddable;

	int		 width;
	int		 height;

	BonoboUIHandler	*uih;

	GtkWidget	*toplevel;
	GtkWidget	*canvas;
};

typedef struct {
	BonoboViewClass parent_class;
} GillViewClass;

GtkType         gill_view_get_type           (void);
BonoboView      *gill_view_new                (EmbeddableGill *embeddable);

/*
 * The GillCanvasItem object
 */
#define GILL_CANVAS_ITEM_TYPE        (gill_canvas_item_get_type ())
#define GILL_CANVAS_ITEM(o)          (GTK_CHECK_CAST ((o), GILL_CANVAS_ITEM_TYPE, GillCanvasItem))
#define GILL_CANVAS_ITEM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GILL_CANVAS_ITEM_TYPE, GillCanvasItemClass))
#define IS_GILL_CANVAS_ITEM(o)       (GTK_CHECK_TYPE ((o), GILL_CANVAS_ITEM_TYPE))
#define IS_GILL_CANVAS_ITEM_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GILL_CANVAS_ITEM_TYPE))

typedef struct _GillCanvasItem GillCanvasItem;

struct _GillCanvasItem {
	BonoboCanvasComponent parent;
	EmbeddableGill *embeddable_gill;
};

typedef struct {
	BonoboCanvasComponentClass parent_class;
} GillCanvasItemClass;

GtkType               gill_canvas_item_get_type (void);
BonoboCanvasComponent *gill_canvas_item_new      (EmbeddableGill *embeddable_gill,
						 GnomeCanvasItem *item);

#endif /* EMBEDDABLE_GILL_H */
