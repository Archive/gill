/*
 * A simple implementation of a Gill Bonobo component.
 *
 * Author:
 *   Nat Friedman (nat@gnome-support.com)
 *
 */
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo.h>
#include "embeddable-gill.h"

CORBA_Environment ev;
CORBA_ORB orb;

static void
init_server_factory (int argc, char **argv)
{
	gnome_CORBA_init_with_popt_table (
		"gill", "0.0",
		&argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);

	orb = gnome_CORBA_ORB ();

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("I could not initialize Bonobo"));
}

int
main (int argc, char **argv)
{
	CORBA_exception_init (&ev);
	
	init_server_factory (argc, argv);

	EmbeddableGillFactory_init ();

	bonobo_main ();

	return 0;
}
