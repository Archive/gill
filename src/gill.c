#include <stdio.h>
#include <gnome.h>
#include "config.h"
#include "gnome-xml/parser.h"
#include "gnome-xml/tree.h"
#include "libgdome/gdome.h"
#include "libgdome/gdome-util.h"
#include "libgdome/gdome-xml.h"
#include "svg.h"
#include "svg-util.h"


  /* destroy all children of root */
  /* awfull hack !!
   */
/* see function reload */



/* forward declarations */

void          print_gdoc_structure         (GdomeNode *node);

int           test_gdome_main              (int argc, 
					    char **argv);

GdomeNode    *traverse_slide               (GdomeNode *node, 
					    int *slide, 
					    int n_slide);

void          connect_path_d_activate      (GtkWidget *entry, 
					    gpointer data);

void          connect_path_d               (GdomeNode *node, 
					    int *slide, 
					    int n_slide, 
					    GtkWidget *entry);

int           gill_hack_keypress           (GtkWidget *widget, 
					    GdkEventKey *event);

void          new_window_svg               (GdomeNode *node);

void          callback                     (GdomeEventListener *self, 
					    GdomeEvent *event, 
					    GdomeException *exc);









/* effective implementation */

void
print_gdoc_structure (GdomeNode *node)
{
  int type;
  GdomeNode *child, *next;
  GdomeDOMString *tagName;
  GdomeException exc = 0;

  type = gdome_n_nodeType (node, &exc);
  if (type == GDOME_TEXT_NODE)
    {
      GdomeDOMString *text;

      text = gdome_cd_data ((GdomeCharacterData *)node, &exc);
      fputs (text->str, stdout);
      gdome_str_unref (text);

      return;
    }
  tagName = NULL;
  if (type == GDOME_ELEMENT_NODE)
    {
      GdomeNamedNodeMap *attrs;
      int i, n_attrs;
      putchar ('<');
      tagName = gdome_el_tagName ((GdomeElement *)node, &exc);
      fputs (tagName->str, stdout);

      attrs = gdome_n_attributes (node, &exc);
      n_attrs = gdome_nnm_length (attrs, &exc);
      for (i = 0; i < n_attrs; i++)
	{
	  GdomeNode *node;
	  GdomeDOMString *attrName, *attrVal;
	  node = gdome_nnm_item (attrs, i, &exc);
	  attrName = gdome_a_name ((GdomeAttr *)node, &exc);
	  attrVal = gdome_a_value ((GdomeAttr *)node, &exc);
	  gdome_n_unref (node, &exc);
	  printf (" %s=\"%s\"", attrName->str, attrVal->str);
	  gdome_str_unref (attrName);
	  gdome_str_unref (attrVal);
	}
      gdome_n_unref ((GdomeNode *)attrs, &exc);
      putchar ('>');
    }
  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      print_gdoc_structure (child);
      next = gdome_n_nextSibling (child, &exc);
      gdome_n_unref (child, &exc);
    }
  if (type == GDOME_ELEMENT_NODE)
    {
      fputs ("</", stdout);
      fputs (tagName->str, stdout);
      gdome_str_unref (tagName);
      putchar ('>');
    }
}



void 
callback (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc)
{
  printf ("Got Event !\n");


}

static void
gdome_test_str_unref (GdomeDOMString *self)
{
  g_free (self->str);
  g_free (self);
}

static GdomeDOMString *
gdome_test_str_mk (const char *str)
{
  GdomeDOMString *result;

  result = g_new (GdomeDOMString, 1);
  result->unref = gdome_test_str_unref;
  result->str = g_strdup (str);
  return result;
}

int
test_gdome_main (int argc, char **argv)
{
  xmlDocPtr doc;
  GdomeDocument *gdoc;
  GdomeElement *el;
  GdomeException exc = 0;
  GdomeNode *ref;
  GdomeElement *empty;
  GdomeEventListener *listener;

  if (argc < 2)
    {
      fprintf (stderr, "usage: test-gdome test.xml\n");
      exit (1);
    }

  doc = xmlParseFile (argv[1]);
  gdoc = gdome_xml_from_document (doc);
  el = gdome_doc_documentElement (gdoc, &exc);

  listener = gdome_xml_evntl_new (callback, NULL);
  gdome_evntt_addEventListener (GDOME_EVNTT (el),
				gdome_test_str_mk ("nodeInsertedIntoSubtree"),
				listener, FALSE, &exc);

  empty = gdome_doc_createElement (gdoc, gdome_test_str_mk ("empty"), &exc);
  ref = gdome_n_firstChild ((GdomeNode *)el, &exc);
  gdome_n_unref (gdome_n_insertBefore ((GdomeNode *)el, (GdomeNode *)empty, ref, &exc), &exc);
  gdome_n_unref ((GdomeNode *)ref, &exc);
  gdome_doc_unref (gdoc, &exc);
  print_gdoc_structure ((GdomeNode *)el);
  gdome_el_unref (el, &exc);
  putchar ('\n');

  xmlFreeDoc (doc);
  return 0;
}

/* This points to our toplevel window */
GtkWidget *app;


/* Callbacks functions */

static void
hello_cb (GtkWidget *widget, void *data)
{
	g_print ("Hello GNOME\n");
	gtk_main_quit ();

	return;
}

static void
quit_cb (GtkWidget *widget, void *data)
{
	gtk_main_quit ();

	return;
}

static GtkWidget *
make_canvas_svg (GdomeNode *node)
{
  GtkWidget *canvas;
  GnomeCanvasGroup *root;
  RenderContext *rc;
  RenderObj *root_ro;
  double x1, y1, x2, y2;

  if (getenv ("REGULAR"))
    canvas = gnome_canvas_new ();
  else
    canvas = gnome_canvas_new_aa ();
  
  gtk_widget_set_usize (canvas, 500, 500);

  root = gnome_canvas_root (GNOME_CANVAS (canvas));

  rc = svg_render_context_new ();
  root_ro = domination_render (rc, root, node);

  gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (root), &x1, &y1, &x2, &y2);
  gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), x1, y1, x2, y2);

  return canvas;
}

static void
zoom_changed (GtkAdjustment *adj, gpointer data)
{
	gnome_canvas_set_pixels_per_unit (data, adj->value);
}

/* traverse a slide */
GdomeNode *
traverse_slide (GdomeNode *node, int *slide, int n_slide)
{
  GdomeNode *n;
  GdomeNodeList *children;
  int i;
  GdomeException exc = 0;

  /* todo: refcnt */
  n = node;
  for (i = 0; i < n_slide; i++)
    {
      if (gdome_n_nodeType (n, &exc) != GDOME_ELEMENT_NODE)
	return NULL;
      children = gdome_n_childNodes (n, &exc);
      n = gdome_nl_item (children, slide[i], &exc);
      gdome_nl_unref (children, &exc);
    }
  return n;
}

void
connect_path_d_activate (GtkWidget *entry, gpointer data)
{
  GdomeNode *node = (GdomeNode *)data;
  GdomeException exc = 0;
  char *str;

  str = gtk_entry_get_text (GTK_ENTRY (entry));
  g_print ("%s\n", str);

  gdome_el_setAttribute ((GdomeElement *) node, 
			 svg_mk_const_gdome_str ("d"),
			 svg_mk_const_gdome_str (str), 
			 &exc);
  
}

/* connect the "d" attribute of a path to the entry */
void
connect_path_d (GdomeNode *node, int *slide, int n_slide, GtkWidget *entry)
{
  GdomeNode *n;
  GdomeDOMString *data;
  GdomeException exc = 0;

  n = traverse_slide (node, slide, n_slide);
  if (n == NULL)
    return;
  if (gdome_n_nodeType (n, &exc) != GDOME_ELEMENT_NODE)
    return;

  data = gdome_el_getAttribute ((GdomeElement *) n, svg_mk_const_gdome_str ("d"),
				&exc);
  if (data)
    {
      gtk_entry_set_text (GTK_ENTRY (entry), data->str);
      gtk_signal_connect (GTK_OBJECT (entry), "activate",
			  GTK_SIGNAL_FUNC (connect_path_d_activate),
			  n);
      gdome_str_unref (data);
    }
}

int n_xml_files;
xmlDocPtr *xml_files;
int cur_xml_file = 0;
GtkWidget *canvas;

static void
reload (void)
{
  GdomeDocument *gdoc;
  GdomeElement *el;
  GdomeException exc = 0;
  GnomeCanvasGroup *root;
  RenderContext *rc;
  RenderObj *root_ro;
  GSList *item_list, *node;

  gdoc = gdome_xml_from_document (xml_files[cur_xml_file]);
  el = gdome_doc_documentElement (gdoc, &exc);

  root = gnome_canvas_root (GNOME_CANVAS (canvas));

  /* destroy all children of root */
  /* awfull hack !!
   */
  g_warning ("This is probably an awfull hack. need to avoid doing this by reading the Canvas' internal fields.");
  item_list = g_slist_copy ((GSList *)GNOME_CANVAS_GROUP (root)->item_list);
  for (node = item_list; node; node = node->next)
    gtk_object_destroy (node->data);
  g_slist_free (item_list);

  rc = svg_render_context_new ();
  root_ro = domination_render (rc, root, (GdomeNode *)el);
}

int
gill_hack_keypress (GtkWidget *widget, GdkEventKey *event)
{

  if (event->keyval == ' ')
   {
     if (cur_xml_file + 1 < n_xml_files)
       {
	 cur_xml_file++;
	 reload ();
       }
   }
  else if (event->keyval == GDK_BackSpace)
    {
     if (cur_xml_file > 0)
       {
	 cur_xml_file--;
	 reload ();
       }
    }
  else
    g_print ("pressed %d\n", event->keyval);
  return TRUE;
}

void
new_window_svg (GdomeNode *node)
{
  GtkWidget *button;
  GtkWidget *vbox;
  GtkWidget *hbox;
  /*  GtkWidget *spin; */
  GtkWidget *table;
  GtkWidget *w;
  GtkWidget *path_entry;
  GtkAdjustment *adj;
  int slide[] = {0, 0};
  
  /*
   * Makes the main window and binds the delete event so you can close
   * the program from your WM
   */
  app = gnome_app_new ("gill", "Gill");
  gtk_widget_realize (app);

  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
		      GTK_SIGNAL_FUNC (quit_cb),
		      NULL);

	
  gtk_signal_connect (GTK_OBJECT (app), "key_press_event",
		      GTK_SIGNAL_FUNC (gill_hack_keypress),
		      NULL);


  /*
   * We make a button, bind the 'clicked' signal to hello and setting it
   * to be the content of the main window
   */
  button = gtk_button_new_with_label ("Quit");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (hello_cb), NULL);
  gtk_container_set_border_width (GTK_CONTAINER (button), 5);

  hbox = gtk_hbox_new (FALSE, 4);
  
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox);

  w = gtk_label_new ("Zoom:");
  gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
  gtk_widget_show (w);

  canvas = make_canvas_svg (node);
  gtk_widget_show (canvas);
  
  adj = GTK_ADJUSTMENT (gtk_adjustment_new (1.00, 0.05, 20.00, 0.05, 0.50, 0.50));
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      (GtkSignalFunc) zoom_changed,
		      canvas);
  w = gtk_spin_button_new (adj, 0.0, 2);
  gtk_widget_set_usize (w, 50, 0);
  gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
  gtk_widget_show (w);
  
  path_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (hbox), path_entry, TRUE, TRUE, 0);
  gtk_widget_show (path_entry);

  connect_path_d (node, slide, sizeof(slide) / sizeof(slide[0]), path_entry);

  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

  table = gtk_table_new (2, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
  gtk_table_attach (GTK_TABLE (table), canvas,
		    0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		    GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		    0, 0);

  w = gtk_hscrollbar_new (GTK_LAYOUT (canvas)->hadjustment);
  gtk_table_attach (GTK_TABLE (table), w,
		    0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		    GTK_FILL,
		    0, 0);
  gtk_widget_show (w);
  
  w = gtk_vscrollbar_new (GTK_LAYOUT (canvas)->vadjustment);
  gtk_table_attach (GTK_TABLE (table), w,
		    1, 2, 0, 1,
		    GTK_FILL,
		    GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		    0, 0);
  gtk_widget_show (w);

  gnome_app_set_contents (GNOME_APP (app), vbox);

  /*
   * We now show the widgets, the order doesn't matter, but i suggests 
   * showing the main window last so the whole window will popup at
   * once rather than seeing the window pop up, and then the button form
   * inside of it. Although with such simple example, you'd never notice.
   */
  gtk_widget_show_all (app);
}

int
main(int argc, char *argv[])
{
  GdomeDocument *gdoc;
  GdomeElement *el;
  GdomeException exc = 0;
  int i;
  char *tmp = NULL;

  gnome_init (PACKAGE, VERSION, argc, argv);
	
  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
  gtk_widget_set_default_visual (gdk_rgb_get_visual ());

  if (argc < 2)
    {
      fprintf (stderr, "usage: %s image.svg\n", argv[0]);
      exit (1);
    }

  set_docbase (tmp = g_dirname (argv[1]));
  g_free (tmp);

  n_xml_files = argc - 1;
  xml_files = g_new (xmlDocPtr, n_xml_files);

  for (i = 0; i < n_xml_files; i++)
    {
     xml_files[i] = xmlParseFile (argv[i + 1]);
    }

  cur_xml_file = 0;
  if (xml_files[0] != NULL)
    {
      gdoc = gdome_xml_from_document (xml_files[0]);
      el = gdome_doc_documentElement (gdoc, &exc);

      new_window_svg ((GdomeNode *)el);

      gtk_main ();

      print_gdoc_structure ((GdomeNode *)el);
      g_print ("\n");
    }
  else
    {
      g_warning ("error reading SVG file %s", argv[1]);
    }

  return 0;
}
