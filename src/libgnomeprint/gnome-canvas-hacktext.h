/* Hacktext item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Copyright (C) 1998,1999 The Free Software Foundation
 *
 * Authors: Federico Mena <federico@nuclecu.unam.mx>
 *          Raph Levien <raph@acm.org>
 */

#ifndef GNOME_CANVAS_HACKTEXT_H
#define GNOME_CANVAS_HACKTEXT_H

BEGIN_GNOME_DECLS


/* Hacktext item for the canvas.  The API is totally unstable - it needs to be replaced with one
 * that supports Unicode and the merged GnomeText/GScript API. However, I need a text item now,
 * and the GnomeText/GScript integration is going to take a bit more effort.
 *
 * The following object arguments are available:
 *
 * name			type			read/write	description
 * ------------------------------------------------------------------------------------------
 * text			char *			RW		The string of the text item.
 * fill_color		string			W		X color specification for fill color,
 *								or NULL pointer for no color (transparent).
 * fill_color_gdk	GdkColor*		RW		Allocated GdkColor for fill.
 * outline_color	string			W		X color specification for outline color,
 *								or NULL pointer for no color (transparent).
 * outline_color_gdk	GdkColor*		RW		Allocated GdkColor for outline.
 * fill_stipple		GdkBitmap*		RW		Stipple pattern for fill
 * outline_stipple	GdkBitmap*		RW		Stipple pattern for outline
 * width_pixels		uint			RW		Width of the outline in pixels.  The outline will
 *								not be scaled when the canvas zoom factor is changed.
 * width_units		double			RW		Width of the outline in canvas units.  The outline
 *								will be scaled when the canvas zoom factor is changed.
 * cap_style		GdkCapStyle		RW		Cap ("endpoint") style for the hacktext.
 * join_style		GdkJoinStyle		RW		Join ("vertex") style for the hacktext.
 */

#define GNOME_TYPE_CANVAS_HACKTEXT            (gnome_canvas_hacktext_get_type ())
#define GNOME_CANVAS_HACKTEXT(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HACKTEXT, GnomeCanvasHacktext))
#define GNOME_CANVAS_HACKTEXT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HACKTEXT, GnomeCanvasHacktextClass))
#define GNOME_IS_CANVAS_HACKTEXT(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HACKTEXT))
#define GNOME_IS_CANVAS_HACKTEXT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HACKTEXT))


typedef struct _GnomeCanvasHacktext GnomeCanvasHacktext;
typedef struct _GnomeCanvasHacktextPriv GnomeCanvasHacktextPriv;
typedef struct _GnomeCanvasHacktextClass GnomeCanvasHacktextClass;

typedef enum {
	GNOME_CANVAS_HACKTEXT_NONZERO,
	GNOME_CANVAS_HACKTEXT_EVENODD,
	GNOME_CANVAS_HACKTEXT_RAW
} GnomeCanvasHacktextWind;

struct _GnomeCanvasHacktext {
	GnomeCanvasItem item;

	char *text;			/* String of the text item */
	double width;			/* Width of hacktext's outline */

	guint fill_color;		/* Fill color, RGBA */
	guint outline_color;		/* Outline color, RGBA */

	gulong fill_pixel;		/* Color for fill */
	gulong outline_pixel;		/* Color for outline */

	GdkBitmap *fill_stipple;	/* Stipple for fill */
	GdkBitmap *outline_stipple;	/* Stipple for outline */

	GdkGC *fill_gc;			/* GC for filling */
	GdkGC *outline_gc;		/* GC for outline */

	guint fill_set : 1;		/* Is fill color set? */
	guint outline_set : 1;		/* Is outline color set? */
	guint width_pixels : 1;		/* Is outline width specified in pixels or units? */

	GdkCapStyle cap;	/* Cap style for outline */
	GdkJoinStyle join;	/* Join style for outline */

	double size;			/* size in user units */

	double x, y;			/* x, y coords of text origin */

	/* Antialiased specific stuff follows */
	guint32 fill_rgba;		/* RGBA color for filling */
	guint32 outline_rgba;		/* RGBA color for outline */

	GnomeCanvasHacktextPriv *priv;	/* Private data */
};

struct _GnomeCanvasHacktextClass {
	GnomeCanvasItemClass parent_class;
};


/* Standard Gtk function */
GtkType gnome_canvas_hacktext_get_type (void);


END_GNOME_DECLS

#endif
