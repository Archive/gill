/* spline.c

   Copyright (C) 1999,2000 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/

/* Currently, this is a testbed for playing with spline editing.

   Once the stuff in here stabilizes, it should move into Gill proper
   as the spline editing subsystem.

   Please ask before editing this file; some of the spline code is likely
   to move into libart.

*/

#include <gnome.h>
#include <stdlib.h>
#include "libgnomeprint/gnome-canvas-bpath-util.h"
#include "libgnomeprint/gnome-canvas-bpath.h"
#include "gnome-canvas-acetate.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CURV mock_curvature

#ifndef M_SQRT2
# define M_SQRT2        1.41421356237309504880  /* sqrt(2) */
#endif

#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

#define M_1_GOLDENMEAN 0.618033988749895 /* (sqrt(5) - 1) / 2 */

#define EPSILON 1e-6

#define noVERBOSE

/* References to MF are to Metafont, the Program, by Donald Knuth,
   Addison-Wesley, 1998. Most references are to section numbers. */

typedef struct _Knot Knot;
typedef union _KnotInfo KnotInfo;
typedef struct _EditablePath EditablePath;
typedef struct _Path Path;
typedef struct _Subpath Subpath;
typedef struct _SubpathInfo SubpathInfo;

/* MF section 256 */
typedef enum {
  KNOT_ENDPOINT,
  KNOT_EXPLICIT,
  KNOT_GIVEN,
  KNOT_CURL,
  KNOT_OPEN
} KnotType;

union _KnotInfo {
  struct {
    double x;
    double y;
  } explicit;
  struct {
    double curl;
    double tension;
  } curl;
  struct {
    double theta;
    double tension;
  } given;
  struct {
    double dummy;
    double tension;
  } open;
};

/* MF section 255, 256 */
struct _Knot {
  double x, y;
  KnotType left_type;
  KnotInfo left_info;
  KnotType right_type;
  KnotInfo right_info;
};

struct _Subpath {
  int n_knot;
  Knot *knot;
};

struct _Path {
  int n_subpath;
  Subpath *subpath;
};

struct _SubpathInfo {
  double d;
  double dx, dy;
  double psi;
  double theta;
};

struct _EditablePath {
  Path *path;
  int n_subpath_max;
  int *n_knot_max;
  gboolean *subpath_sel;
  gboolean **knot_sel;
};

typedef struct _SplineCtx SplineCtx;
typedef struct _BpathEdit BpathEdit;

struct _SplineCtx {
  GtkWidget *app;
  GtkWidget *canvas;

  BpathEdit *edit;
  GnomeCanvasItem *bpath;
};

struct _BpathEdit {
  GnomeCanvasGroup *group;
  GnomeCanvasItem *bpath;
  GnomeCanvasItem *acetate;
  EditablePath *path;

  int button;
  int sp_idx;
  int knot_idx;

  double x_ofs, y_ofs;

  int n_knot;
  int n_knot_max;
  GnomeCanvasItem **knot;
};

static void bpath_edit_update (BpathEdit *edit);

/* MF section 116 */
static double
velocity (double theta, double phi, double tau)
{
  double s_t;
  double c_t;
  double s_f;
  double c_f;
  double num, denom;

#ifdef VERBOSE
  printf ("%% velocity: theta = %g, phi = %g, tau = %g\n", theta, phi, tau);
#endif
  s_t = sin (theta);
  s_f = sin (phi);
  c_t = cos (theta);
  c_f = cos (phi);
  num = 2 + M_SQRT2 * (s_t - 0.0625 * s_f) * (s_f - 0.0625 * s_t) *
    (c_t - c_f);
  denom = tau * 3 * (1 + M_1_GOLDENMEAN * c_t + (1 - M_1_GOLDENMEAN) * c_f);
#ifdef VERBOSE
  printf ("%% velocity: num = %g, denom = %g\n", num, denom);
#endif
  if (num > denom * 4)
    return 4.0;
  else
    return num / denom;
}

/**
 * curl_ends: Set open endpoints to curl.
 * @subpath: The #Subpath.
 *
 * This function allows input paths to be specified with open endpoints,
 * which get replaced with a curl of 1.0. This simplifies editing quite
 * a bit, as "smooth" endpoints are stored with OPEN, while "corner"
 * endpoints with CURL.
 **/
static void
curl_ends (Subpath *subpath)
{
  Knot *knot;
  int i;

  for (i = 0; i < subpath->n_knot; i++)
    {
      knot = &subpath->knot[i];
      if (knot->left_type == KNOT_ENDPOINT &&
	  knot->right_type == KNOT_OPEN)
	{
	  knot->right_type = KNOT_CURL;
	  knot->right_info.curl.curl = 1.0;
	}
      else if (knot->right_type == KNOT_ENDPOINT &&
	       knot->left_type == KNOT_OPEN)
	{
	  knot->left_type = KNOT_CURL;
	  knot->left_info.curl.curl = 1.0;
	}
    }
}

/* MF section 271 */
static void
join_knots (Subpath *subpath)
{
  int i;
  Knot *p, *q;

  for (i = 1; i < subpath->n_knot; i++)
    {
      p = &subpath->knot[i - 1];
      q = &subpath->knot[i];
      if (fabs (p->x - q->x) < EPSILON &&
	  fabs (p->y - q->y) < EPSILON)
	{
	  /* knots coincide */
	  if (p->right_type > KNOT_EXPLICIT)
	    {
	      p->right_type = KNOT_EXPLICIT;
	      if (p->left_type == KNOT_OPEN)
		{
		  p->left_type = KNOT_CURL;
		  p->left_info.curl.curl = 1.0;
		}
	      q->left_type = KNOT_EXPLICIT;
	      if (q->right_type == KNOT_OPEN)
		{
		  q->right_type = KNOT_CURL;
		  q->right_info.curl.curl = 1.0;
		}
	      p->right_info.explicit.x = p->x;
	      p->right_info.explicit.y = p->y;
	      q->left_info.explicit.x = p->x;
	      q->left_info.explicit.y = p->y;
	    }
	}
    }
}

/* MF section 274 */
static double
curvature (double alpha, double beta, double theta, double phi, double d)
{
  double rho, sigma;

#ifdef VERBOSE
  printf ("%% curvature: alpha = %g, beta = %g, theta = %g, phi = %g, d = %g\n",
	  alpha, beta, theta, phi, d);
#endif
  rho = alpha * velocity (theta, phi, 1.0 / 3.0);
  sigma = beta * velocity (phi, theta, 1.0 / 3.0);
  return (2 * sigma * sin (theta + phi) - 6 * sin (theta)) / (rho * rho * d);
}

static double
mock_curvature (double alpha, double beta, double theta, double phi, double d)
{
  /* thus, the derivative of mock curvature with respect to theta is:
     - 4 * beta / (alpha * alpha * d)
  */

#ifdef VERBOSE
  printf ("%% mock_curvature: alpha = %g, beta = %g, theta = %g, phi = %g, d = %g\n",
	  alpha, beta, theta, phi, d);
#endif

  return (2 * beta * (theta + phi) - 6 * theta) / (alpha * alpha * d);
}

/* MF section 281 */
static SubpathInfo *
subpath_info (Subpath *subpath, int k0, int n)
{
  SubpathInfo *result;
  int i;
  int n_info;
  int next;
  double dx, dy;
  double last_psi;
  double tmp;
  int n_knot;

  n_knot = subpath->n_knot;

  n_info = n;
  if (subpath->knot[k0].left_type != KNOT_OPEN)
    n_info++;

  result = malloc (n_info * sizeof(SubpathInfo));
  for (i = 0; i < n_info; i++)
    {
      next = (k0 + i + 1) % n_knot;
      dx = subpath->knot[next].x - subpath->knot[(i + k0) % n_knot].x;
      dy = subpath->knot[next].y - subpath->knot[(i + k0) % n_knot].y;
      result[i].dx = dx;
      result[i].dy = dy;
      result[i].d = sqrt (dx * dx + dy * dy);
      result[i].psi = atan2 (dy, dx);
    }

  if (n < n_knot || subpath->knot[k0].left_type != KNOT_OPEN)
    last_psi = result[0].psi;
  else
    last_psi = result[n_knot - 1].psi;
  for (i = 0; i < n; i++)
    {
      next = (i + 1) % n_knot;
      tmp = result[i].psi;
      result[i].psi = tmp - last_psi;
      if (result[i].psi > M_PI)
	result[i].psi -= 2 * M_PI;
      else if (result[i].psi < -M_PI)
	result[i].psi += 2 * M_PI;
      last_psi = tmp;
    }
  if (n < n_info)
    result[n].psi = 0;

  return result;
}

/* assigns the control points to a spline section. dst0 and dst1 are
   adjacent knots, while src0 and src1 are the corresponding (also
   adjacent) knots. Sets dst0's right info and dst1's left. */
static void
assign_control_points (Knot *dst0, Knot *dst1, Knot *src0, Knot *src1, SubpathInfo *info0, SubpathInfo *info1)
{
  double theta0, phi0, psi0;
  double theta1, phi1, psi1;
  double c_t, s_t;
  double c_f, s_f;
  double rho, sigma;
  double dx, dy;

  theta0 = info0->theta;
  psi0 = info0->psi;
  phi0 = -(theta0 + psi0);

  theta1 = info1->theta;
  psi1 = info1->psi;
  phi1 = -(theta1 + psi1);

  rho = velocity (theta0, phi1, src0->right_info.open.tension);
  sigma = velocity (phi1, theta0, src1->left_info.open.tension);

#ifdef VERBOSE
  printf ("theta0 = %g, phi1 = %g, rho = %g, sigma = %g\n",
	  theta0, phi1, rho, sigma);
#endif

  c_t = rho * cos (theta0);
  s_t = rho * sin (theta0);
  c_f = sigma * cos (phi1);
  s_f = sigma * sin (phi1);

  dx = info0->dx;
  dy = info0->dy;

#ifdef VERBOSE
  g_print ("dx = %g, dy = %g\n", dx, dy);
#endif

  dst0->right_info.explicit.x = src0->x + dx * c_t - dy * s_t;
  dst0->right_info.explicit.y = src0->y + dx * s_t + dy * c_t;

  dst1->left_info.explicit.x = src1->x - dx * c_f - dy * s_f;
  dst1->left_info.explicit.y = src1->y + dx * s_f - dy * c_f;

}

static Subpath *
initial_path (Subpath *src, int k0, int n, SubpathInfo *info)
{
  Subpath *result;
  Knot *new_knots;
  int i, i_next;
  int n_dst;
  int k_src, k_next;
  int n_knot;

  n_knot = src->n_knot;
  result = malloc (sizeof (Subpath));
  n_dst = n;
  if (src->knot[k0].left_type != KNOT_OPEN)
    n_dst++;
  new_knots = malloc (n_dst * sizeof (Knot));

  result->n_knot = n_dst;
  result->knot = new_knots;

  for (i = 0; i < n_dst; i++)
    {
      k_src = (i + k0) % n_knot;
      new_knots[i].x = src->knot[k_src].x;
      new_knots[i].y = src->knot[k_src].y;
      new_knots[i].left_type = KNOT_EXPLICIT;
      new_knots[i].right_type = KNOT_EXPLICIT;

      info[i].theta = -0.5 * info[i].psi; /* initial value for theta */

      if (i < n || src->knot[k0].left_type == KNOT_OPEN)
	{
	  i_next = (i + 1) % n_knot;
	  k_next = (k_src + 1) % n_knot;
	  assign_control_points (&new_knots[i], &new_knots[i_next],
				 &src->knot[k_src], &src->knot[k_next],
				 &info[i], &info[i_next]);
	}
    }

  if (src->knot[k0].left_type == KNOT_ENDPOINT)
    new_knots[0].left_type = KNOT_ENDPOINT;

  if (src->knot[(k0 + n_dst - 1) % n_knot].right_type == KNOT_ENDPOINT)
    new_knots[n_dst - 1].right_type = KNOT_ENDPOINT;

  return result;
}

/* computes curvatures from info */
static void
compute_curvatures (Knot *knots, int n_knot, SubpathInfo *info)
{
  int i, prev, next;
  double left_curv, right_curv;
  double theta, c_t, s_t;

  for (i = 0; i < n_knot; i++)
    {
      prev = (i + n_knot - 1) % n_knot;
      next = (i + 1) % n_knot;
      right_curv = curvature (1.0 / knots[i].right_info.open.tension,
			     1.0 / knots[next].left_info.open.tension,
			     info[i].theta,
			     -(info[next].psi + info[next].theta),
			     info[i].d);
      left_curv = curvature (1.0 / knots[i].left_info.open.tension,
			     1.0 / knots[prev].right_info.open.tension,
			     -(info[i].psi + info[i].theta),
			     info[prev].theta,
			     info[prev].d);
      theta = atan2 (info[i].dy, info[i].dx) + info[i].theta;
      c_t = cos (theta);
      s_t = sin (theta);
      if (fabs (s_t) < 0.5)
	s_t = s_t > 0 ? 0.5 : -0.5;
#ifdef VERBOSE
      printf ("%g %g moveto (%.2g) show %% left curv\n",
	      knots[i].x - 10 * c_t,
	      knots[i].y - 10 * s_t - 5,
	      left_curv);
      printf ("%g %g moveto (%.2g) show %% right curv\n",
	      knots[i].x + 10 * c_t,
	      knots[i].y + 10 * s_t - 5,
	      right_curv);
#if 0
      printf ("(knot %d: left curv = %g, right_curv = %g) show\n", i,
	      left_curv, right_curv);
#endif
#endif
    }
}

/* Iterate towards satisfying curvature continuity */
static void
adjust_thetas (Subpath *subpath, int k0, int n, SubpathInfo *info)
{
  int i;
  int i_beg, i_end;
  int i_prev, i_next;
  int prev, this, next;
  double left_curv, right_curv;
  double *adj;
  double tens_r, tens_l;
  double tens_r_prev, tens_l_next;
  double dcurv;
  int n_adj;
  int n_knot;

#ifdef VERBOSE
  printf ("adjust_thetas: k0= %d, n = %d\n", k0, n);
#endif

  n_knot = subpath->n_knot;

  if (subpath->knot[k0].left_type != KNOT_OPEN)
    {
      /* adjust open curve */
      i_beg = 1;
      i_end = n;
      n_adj = n + 1;
    }
  else
    {
      i_beg = 0;
      i_end = n;
      n_adj = n;
    }

  adj = malloc (n_adj * sizeof (double));

  for (i = i_beg; i < i_end; i++)
    {
      prev = (k0 + i + n_knot - 1) % n_knot;
      this = (k0 + i) % n_knot;
      next = (k0 + i + 1) % n_knot;

      i_prev = (i + n_knot - 1) % n_knot;
      i_next = (i + 1) % n_knot;
      tens_r = subpath->knot[this].right_info.open.tension;
      tens_l = subpath->knot[this].left_info.open.tension;
      tens_r_prev = subpath->knot[prev].right_info.open.tension;
      tens_l_next = subpath->knot[next].left_info.open.tension;
      right_curv = CURV (1.0 / tens_r,
			 1.0 / tens_l_next,
			 info[i].theta,
			 -(info[i_next].psi + info[i_next].theta),
			 info[i].d);
      left_curv = CURV (1.0 / tens_l,
			1.0 / tens_r_prev,
			-(info[i].psi + info[i].theta),
			info[i_prev].theta,
			info[i_prev].d);

      /* Approximate the first derivative of right_curv - left_curv
	 with respect to theta, and use Newton-Raphson style
	 iteration. */
      dcurv = 4 *
	(tens_r * tens_r / (tens_l_next * info[i].d) +
	 tens_l * tens_l / (tens_r_prev * info[i_prev].d));
#ifdef VERBOSE
      printf ("%% dcurv = %g\n", dcurv);
#endif
      adj[i] = 0.5 * (right_curv - left_curv) / dcurv;
      if (fabs (adj[i]) > 0.1)
	adj[i] = adj[i] > 0 ? 0.1 : -0.1;
#ifdef VERBOSE
      printf ("%% adj[%d] = %g, rc = %g, lc = %g, th = %g, psi = %g\n",
	      i, adj[i], right_curv, left_curv, info[i].theta, info[i].psi);
#endif
    }

  if (subpath->knot[k0].left_type != KNOT_OPEN)
    {
      /* adjust curls at endpoints */
      /* todo: handle GIVEN and EXPLICIT as well */

#if 0
      if (fabs (adj[i]) > 0.1)
	adj[i] = adj[i] > 0 ? 0.1 : -0.1;
#endif


      tens_r = subpath->knot[k0].right_info.curl.tension;
      tens_l_next = subpath->knot[(k0 + 1) % n_knot].left_info.open.tension;
      right_curv = CURV (1.0 / tens_r,
			 1.0 / tens_l_next,
			 info[0].theta,
			 -(info[1].psi + info[1].theta),
			 info[0].d);
      left_curv = CURV (1.0 / tens_l_next,
			1.0 / tens_r,
			-(info[1].psi + info[1].theta),
			info[0].theta,
			info[0].d);

      /* Approximate the first derivative of right_curv - left_curv
	 with respect to theta, and use Newton-Raphson style
	 iteration. */
      dcurv = 4 *
	(tens_r * tens_r / (tens_l_next * info[0].d) +
	 tens_l_next * tens_l_next / (tens_r * info[0].d));

      adj[0] = 1.0 * (right_curv - left_curv) / dcurv;

      tens_l = subpath->knot[(k0 + n) % n_knot].left_info.curl.tension;
      tens_r_prev = subpath->knot[(k0 + n - 1) % n_knot].right_info.open.tension;
      right_curv = CURV (1.0 / tens_r_prev,
			1.0 / tens_l,
			info[n - 1].theta,
			 -(info[n].psi + info[n].theta),
			info[n - 1].d);
      left_curv = CURV (1.0 / tens_l,
			 1.0 / tens_r_prev,
			 -(info[n].psi + info[n].theta),
			 info[n - 1].theta,
			 info[n - 1].d);

#ifdef VERBOSE
      printf ("th_(n-1) = %g, ph_n = %g, ps_n = %g\n",
	      info[n-1].theta,
	      -(info[n].psi + info[n].theta), info[n].psi);
#endif

      /* Approximate the first derivative of right_curv - left_curv
	 with respect to theta, and use Newton-Raphson style
	 iteration. */
      dcurv = 4 *
	(tens_l * tens_l / (tens_r_prev * info[n - 1].d) +
	 tens_r_prev * tens_r_prev / (tens_l * info[n - 1].d));

#ifdef VERBOSE
      g_print ("tens_l = %g, tens_r_prev = %g, info[%d - 1].d = %g\n",
	       tens_l, tens_r_prev, n, info[n - 1].d);
      g_print ("dcurv = %g\n", dcurv);
#endif

      adj[n] = 1.0 * (right_curv - left_curv) / dcurv;

#ifdef VERBOSE
      printf ("%% left_curv = %g, right_curv = %g, adj = %g\n",
	      left_curv, right_curv, adj[n]);
#endif

    }

  for (i = 0; i < n_adj; i++)
    info[i].theta += adj[i];

  free (adj);
}

static void
assign_subpath_control_points (Knot *dst, Subpath *src, int k0, int n, SubpathInfo *info)
{
  int i, i_next;
  int k, k_next;
  int n_knot;

  n_knot = src->n_knot;
  for (i = 0; i < n; i++)
    {
      k = (i + k0) % n_knot;
      i_next = (i + 1) % n_knot;
      k_next = (k + 1) % n_knot;
      assign_control_points (&dst[i], &dst[i_next], &src->knot[k], &src->knot[k_next], &info[i], &info[i_next]);
#ifdef VERBOSE
      g_print ("info[%d].psi = %g (next %g)\n",
	       i, info[i].psi, info[i_next].psi);
#endif
    }
}

static void
fill_in_knots (Subpath *subpath, int k0, int n)
{
  SubpathInfo *info;
  Subpath *initial;
  int i;
  int n_knot;
  int k1;

  n_knot = subpath->n_knot;

  if (n == 0)
    return;

  if (n == 1)
    {
      int k1 = (k0 + 1) % n_knot;
      subpath->knot[k0].right_type = KNOT_EXPLICIT;
      subpath->knot[k0].right_info.explicit.x = subpath->knot[k0].x;
      subpath->knot[k0].right_info.explicit.y = subpath->knot[k0].y;
      subpath->knot[k1].left_type = KNOT_EXPLICIT;
      subpath->knot[k1].left_info.explicit.x = subpath->knot[k1].x;
      subpath->knot[k1].left_info.explicit.y = subpath->knot[k1].y;
      return;
    }

  printf ("fill_in_knots: k0 = %d, n = %d, n_knot = %d\n",
	  k0, n, subpath->n_knot);
  info = subpath_info (subpath, k0, n);
  initial = initial_path (subpath, k0, n, info);

#if 0
  printf ("1 0 0 setrgbcolor\n");
  compute_curvatures (knots, n_knot, info);
#endif

  for (i = 0; i < 10; i++)
    adjust_thetas (subpath, k0, n, info);

  assign_subpath_control_points (initial->knot, subpath, k0, n, info);

#ifdef VERBOSE
  printf ("0 0 1 setrgbcolor\n");
#endif
#if 0
  compute_curvatures (knots, n_knot, info);
#endif

  if (subpath->knot[k0].left_type == KNOT_OPEN)
    {
      /* is this branch needed at all? */
      for (i = 0; i < n; i++)
	{
	  subpath->knot[(i + k0) % n_knot] = initial->knot[i];
	}
    }
  else
    {
      subpath->knot[k0].right_type = initial->knot[0].right_type;
      subpath->knot[k0].right_info = initial->knot[0].right_info;
      for (i = 1; i < n; i++)
	{
	  subpath->knot[(i + k0) % n_knot] = initial->knot[i];
	}
      k1 = (n + k0) % n_knot;
      subpath->knot[k1].left_type = initial->knot[n].left_type;
      subpath->knot[k1].left_info = initial->knot[n].left_info;
    }

#ifdef VERBOSE
  printf ("0 0 0 setrgbcolor\n");
#endif

  free (initial->knot);
  free (initial);
  free (info);
}

/* Find the first breakpoint >= k0, or subpath->n_knot if no such breakpoint
   is to be found.
*/
/**
 * find_breakpoint: Find breakpoint in knot representation.
 * @subpath: Subpath in which to find breakpoint.
 * @k0: Knot number to begin search.
 *
 * Finds breakpoint beginning at k0. A breakpoint knot is basically
 * any knot with a type other than OPEN.
 *
 * Return value: Knot number of breakpoint found.
 **/
static int
find_breakpoint (Subpath *subpath, int k0)
{
  int k;
  int n_knot = subpath->n_knot;

  for (k = k0; k < k0 + n_knot; k++)
    if (subpath->knot[k % n_knot].left_type != KNOT_OPEN)
      return k % n_knot;
  return n_knot;
}

/* MF section 269, 272, 273 */
static void
make_choices_subpath (Subpath *subpath)
{
  int k0, k1, k_first;
  int n_knot;

  n_knot = subpath->n_knot;

  curl_ends (subpath);
  join_knots (subpath);
  for (k0 = 0; k0 < subpath->n_knot; k0++)
    g_print ("knot %d: left = %d, right=%d\n", k0,
	     subpath->knot[k0].left_type, subpath->knot[k0].right_type);

  k_first = find_breakpoint (subpath, 0);
  g_print ("k_first = %d\n", k_first);
  if (k_first == n_knot)
    {
      /* an cyclic curve of all "open" points */
      fill_in_knots (subpath, 0, n_knot);
    }
  else
    {
      /* there are breakpoints */
      k1 = k_first;
      do {
	k0 = k1;
	k1 = find_breakpoint (subpath, (k0 + 1) % n_knot);
	g_print ("k0 = %d, k1 = %d, left_type = %d\n", k0, k1,
		 subpath->knot[k1].left_type);
	if (subpath->knot[k1].left_type != KNOT_ENDPOINT)
	  fill_in_knots (subpath, k0, 1 + (k1 + n_knot - k0 - 1) % n_knot);
      } while (k1 != k_first);
    }

}

/*
  Replace all knots in the path with EXPLICIT control points.

  MF section 269 */
static void
make_choices (Path *path) {
  int i;

  for (i = 0; i < path->n_subpath; i++)
    make_choices_subpath (&path->subpath[i]);
}

/* for now, only output explicit path */
static void
output_path_as_ps (Path *path)
{
  int i, j;
  Subpath *subpath;

  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      printf ("%g %g moveto\n", subpath->knot[0].x, subpath->knot[0].y);
      for (j = 0; j < subpath->n_knot; j++)
	{
	  int next;

	  next = (j + 1) % subpath->n_knot;
	  printf ("%g %g %g %g %g %g curveto\n",
		  subpath->knot[j].right_info.explicit.x,
		  subpath->knot[j].right_info.explicit.y,
		  subpath->knot[next].left_info.explicit.x,
		  subpath->knot[next].left_info.explicit.y,
		  subpath->knot[next].x, subpath->knot[next].y);
	}
    }
  printf ("1 setlinewidth stroke\n");
  printf ("0.25 setlinewidth\n");

  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      for (j = 0; j < subpath->n_knot; j++)
	{
	  printf ("%g %g ci\n", subpath->knot[j].x, subpath->knot[j].y);
	  printf ("%g %g moveto %g %g lineto stroke\n",
		  subpath->knot[j].x, subpath->knot[j].y,
		  subpath->knot[j].right_info.explicit.x,
		  subpath->knot[j].right_info.explicit.y);
	  printf ("%g %g moveto %g %g lineto stroke\n",
		  subpath->knot[j].x, subpath->knot[j].y,
		  subpath->knot[j].left_info.explicit.x,
		  subpath->knot[j].left_info.explicit.y);
	}
    }
}

static GnomeCanvasBpathDef *
convert_path_to_bpath_def (Path *path)
{
  GnomeCanvasBpathDef *result;
  int i, j;
  Subpath *subpath;
  int n;

  result = gnome_canvas_bpath_def_new ();

#if 0
  /* helpful for electric fence with current canvas */
  gnome_canvas_bpath_def_moveto (result, 10, 10);
  gnome_canvas_bpath_def_lineto (result, 30, 20);
#endif

  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      gnome_canvas_bpath_def_moveto (result,
				     subpath->knot[0].x, subpath->knot[0].y);
      n = subpath->n_knot;
#ifdef VERBOSE
      for (j = 0; j < n; j++)
	{
	  printf ("knot[%d].x = %g, .y = %g\n",
		  j, subpath->knot[j].x, subpath->knot[j].y);
	  printf ("knot[%d].left_type = %d, .right_type = %d\n",
		  j, subpath->knot[j].left_type, subpath->knot[j].right_type);
	  if (subpath->knot[j].left_type == KNOT_EXPLICIT)
	    printf ("knot[%d].left_info.explicit.x = %g, .y = %g\n",
		    j, subpath->knot[j].left_info.explicit.x, subpath->knot[j].left_info.explicit.y);
	  if (subpath->knot[j].right_type == KNOT_EXPLICIT)
	    printf ("knot[%d].right_info.explicit.x = %g, .y = %g\n",
		    j, subpath->knot[j].right_info.explicit.x, subpath->knot[j].right_info.explicit.y);
	}
#endif
      if (subpath->knot[0].left_type == KNOT_ENDPOINT)
	n--;
      for (j = 0; j < n; j++)
	{
	  int next;

	  next = (j + 1) % subpath->n_knot;
	  gnome_canvas_bpath_def_curveto (result,
					  subpath->knot[j].right_info.explicit.x,
					  subpath->knot[j].right_info.explicit.y,
					  subpath->knot[next].left_info.explicit.x,
					  subpath->knot[next].left_info.explicit.y,
					  subpath->knot[next].x,
					  subpath->knot[next].y);
	}
      if (subpath->knot[0].left_type != KNOT_ENDPOINT)
	gnome_canvas_bpath_def_closepath (result);
    }

  return result;
}

static Path *
path_copy (Path *path)
{
  Path *result;
  int i, j;
  Subpath *subpath, *new_subpath;
  int n_subpath_max;

  result = malloc (sizeof (Path));

  result->n_subpath = path->n_subpath;
  if (path->n_subpath == 0)
    n_subpath_max = 1;
  else
    n_subpath_max = path->n_subpath;
  new_subpath = malloc (sizeof (Subpath) * n_subpath_max);
  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      new_subpath[i].n_knot = subpath->n_knot;
      new_subpath[i].knot = malloc (sizeof (Knot) * subpath->n_knot);
      for (j = 0; j < subpath->n_knot; j++)
	new_subpath[i].knot[j] = subpath->knot[j];
    }
  result->subpath = new_subpath;

  return result;
}

static void
path_free (Path *path)
{
  int i;

  for (i = 0; i < path->n_subpath; i++)
    free (path->subpath[i].knot);
  free (path->subpath);
  free (path);
}

static void
quit_cb (GtkWidget *widget, void *data)
{
	gtk_main_quit ();

	return;
}


static void
closest_knot (Path *path, int *p_subpath, int *p_knot, double x, double y)
{
  int best_sp, best_k;
  double best_dd;
  int i, j;
  Subpath *subpath;
  double dx, dy, dd;

  best_sp = 0;
  best_k = 0;
  best_dd = 1e12;

  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      for (j = 0; j < subpath->n_knot; j++)
	{
	  dx = x - subpath->knot[j].x;
	  dy = y - subpath->knot[j].y;
	  dd = dx * dx + dy * dy;
	  if (dd < best_dd)
	    {
	      best_dd = dd;
	      best_sp = i;
	      best_k = j;
	    }
	}
    }

  *p_subpath = best_sp;
  *p_knot = best_k;
}

/**
 * insert_knot: Insert a new knot into an editable path.
 * @path: The EditablePath.
 * @subpath_idx: Subpath index within @path.
 * @knot_idx: Knot index within the subpath.
 * @x, @y: x, y coordinates of new knot.
 * @corner: TRUE if the knot is a corner.
 **/
static void
insert_knot (EditablePath *path, int subpath_idx, int knot_idx,
	     double x, double y, gboolean corner)
{
  Subpath *sp;
  int i;
  gboolean *ksel;
  Knot *knot;

  sp = &path->path->subpath[subpath_idx];
  ksel = path->knot_sel[subpath_idx];
  if (sp->n_knot == path->n_knot_max[subpath_idx])
    {
      path->n_knot_max[subpath_idx] <<= 1;
      sp->knot = g_renew (Knot, sp->knot, path->n_knot_max[subpath_idx]);
      ksel = g_renew (gboolean, ksel, path->n_knot_max[subpath_idx]);   
      path->knot_sel[subpath_idx] = ksel;
    }
  for (i = sp->n_knot; i > knot_idx; i--)
    {
      sp->knot[i] = sp->knot[i - 1];
      ksel[i] = ksel[i - 1];
    }
  knot = &sp->knot[knot_idx];
  if (knot_idx == 0)
    {
      knot->left_type = KNOT_ENDPOINT;
      if (sp->n_knot)
	{
	  if (knot[1].right_type == KNOT_CURL)
	    {
	      knot[1].left_type = KNOT_CURL;
	      knot[1].left_info.curl.tension = 1.0;
	      knot[1].left_info.curl.curl = 1.0;
	    }
	  else
	    {
	      knot[1].left_type = KNOT_OPEN;
	      knot[1].left_info.open.tension = 1.0;
	    }
	}
    }
  else
    {
      if (corner)
	{
	  knot->left_type = KNOT_CURL;
	  knot->left_info.curl.tension = 1.0;
	  knot->left_info.curl.curl = 1.0;
	}
      else
	{
	  knot->left_type = KNOT_OPEN;
	  knot->left_info.open.tension = 1.0;
	}
    }
  if (knot_idx == sp->n_knot)
    {
      knot->right_type = KNOT_ENDPOINT;
      if (knot_idx > 0)
	{
	  if (knot[-1].left_type == KNOT_CURL)
	    {
	      knot[-1].right_type = KNOT_CURL;
	      knot[-1].right_info.curl.tension = 1.0;
	      knot[-1].right_info.curl.curl = 1.0;
	    }
	  else
	    {
	      knot[-1].right_type = KNOT_OPEN;
	      knot[-1].right_info.open.tension = 1.0;
	    }
	}
    }
  else
    {
      if (corner)
	{
	  knot->right_type = KNOT_CURL;
	  knot->right_info.curl.tension = 1.0;
	  knot->right_info.curl.curl = 1.0;
	}
      else
	{
	  knot->right_type = KNOT_OPEN;
	  knot->right_info.open.tension = 1.0;
	}
    }

  knot->x = x;
  knot->y = y;
  ksel[knot_idx] = TRUE;
  sp->n_knot++;
}

/* Select the given knot. */
static void
select_knot (BpathEdit *edit, int subpath_idx, int knot_idx)
{
  int i;
  EditablePath *epath;

  epath = edit->path;

  for (i = 0; i < epath->path->n_subpath; i++)
    epath->subpath_sel[i] = (i == subpath_idx);
  for (i = 0; i < epath->path->subpath[subpath_idx].n_knot; i++)
    epath->knot_sel[subpath_idx][i] = (i == knot_idx);

  /* These will become obsolete */
  edit->sp_idx = subpath_idx;
  edit->knot_idx = knot_idx;
}

/* Toggle corner status on selected knot. */
static void
toggle_corner (BpathEdit *edit, int subpath_idx, int knot_idx)
{
  Path *path = edit->path->path;
  Knot *knot;

  knot = &path->subpath[subpath_idx].knot[knot_idx];
  if (knot->left_type == KNOT_OPEN)
    {
      knot->left_type = KNOT_CURL;
      knot->left_info.curl.curl = 1.0;
    }
  else if (knot->left_type == KNOT_CURL)
    {
      knot->left_type = KNOT_OPEN;
    }
  if (knot->right_type == KNOT_OPEN)
    {
      knot->right_type = KNOT_CURL;
      knot->right_info.curl.curl = 1.0;
    }
  else if (knot->right_type == KNOT_CURL)
    {
      knot->right_type = KNOT_OPEN;
    }
}

/* Close an open subpath. */
static void
close_subpath (BpathEdit *edit, int subpath_idx)
{
  Path *path = edit->path->path;
  Subpath *subpath = &path->subpath[subpath_idx];
  Knot *knot_beg, *knot_end;

  if (subpath->n_knot < 3)
    return;
  knot_beg = &subpath->knot[0];
  knot_end = &subpath->knot[subpath->n_knot - 1];
  if (knot_beg->left_type == KNOT_ENDPOINT &&
      knot_end->right_type == KNOT_ENDPOINT)
    {
      printf ("----close-----\n");
      knot_beg->left_type = KNOT_OPEN;
      knot_beg->left_info.open.tension = 1.0;
      knot_beg->right_type = KNOT_OPEN;
      knot_beg->right_info.open.tension = 1.0;
      knot_end->left_type = KNOT_OPEN;
      knot_end->left_info.open.tension = 1.0;
      knot_end->right_type = KNOT_OPEN;
      knot_end->right_info.open.tension = 1.0;
    }
}

static int
acetate_event (GnomeCanvasItem *item, GdkEvent *event, BpathEdit *edit)
{
  double x, y;
  EditablePath *path;
  int i;
  int n_sel;
  gboolean new_path;
  int subpath_idx, knot_idx;
  int status;
  gboolean corner;

  /* This happens whenever we get a click on the background */
  switch (event->type) {
  case GDK_BUTTON_PRESS:
    edit->button = TRUE;
    x = event->button.x;
    y = event->button.y;
    corner = (event->button.button == 2);
    path = edit->path;
    n_sel = 0;
    subpath_idx = 0; /* just to avoid uninitialized var warning */
    knot_idx = 0; /* ditto */
    for (i = 0; i < path->path->n_subpath; i++)
      {
	if (path->subpath_sel[i])
	  {
	    n_sel++;
	    subpath_idx = i;
	  }
      }
    if (n_sel == 1 && path->path->subpath[subpath_idx].knot[0].left_type == KNOT_ENDPOINT)
      {
	/* one path selected, and it's a non-closed path (and, in a
	   correctness-deficient way, the breakpoints are at 0 and
	   n-1) */
	int j;
	gboolean *ks = path->knot_sel[subpath_idx];
	int n_knot = path->path->subpath[subpath_idx].n_knot;

	for (j = 0; j < n_knot; j++)
	  g_print (" %d", ks[j]);
	g_print ("\n");
	if (n_knot < 2 || ks[0] != ks[n_knot - 1])
	  {
	    new_path = FALSE;
	    for (j = 1; j < n_knot - 1; j++)
	      if (ks[j])
		{
		  /* New path if any interior knots are selected */
		  new_path = TRUE;
		  break;
		}
	    if (ks[n_knot - 1])
	      {
		ks[n_knot - 1] = FALSE;
		knot_idx = n_knot;
	      }
	    else
	      {
		ks[0] = FALSE;
		knot_idx = 0;
	      }
	  }
	else
	  /* New path if both endknots either selected or not selected */
	  new_path = TRUE;
      }
    else
      /* New path if zero or two or more paths selected */
      new_path = TRUE;
    if (new_path)
      {
	Subpath *new_subpath;
	/* Create a new subpath */
	subpath_idx = path->path->n_subpath;
	if (path->n_subpath_max == subpath_idx)
	  {
	    path->n_subpath_max <<= 1;
	    path->path->subpath = g_renew (Subpath,
					   path->path->subpath,
					   path->n_subpath_max);
	    path->n_knot_max = g_renew (int, path->n_knot_max,
					path->n_subpath_max);
	    path->knot_sel = g_renew (int *, path->knot_sel,
				      path->n_subpath_max);
	  }
	new_subpath = &path->path->subpath[subpath_idx];
	new_subpath->n_knot = 0;
	path->n_knot_max[subpath_idx] = 1;
	new_subpath->knot = g_new (Knot, path->n_knot_max[subpath_idx]);
	path->subpath_sel[subpath_idx] = TRUE;
	path->knot_sel[subpath_idx] = g_new (gboolean,
					     path->n_knot_max[subpath_idx]);
	path->path->n_subpath++;
	knot_idx = 0;
      }
    insert_knot (path, subpath_idx, knot_idx, x, y, corner);
    g_print ("%d selected, sub_idx = %d, new_path = %d\n",
	     n_sel, subpath_idx, new_path);
#if 0
    g_print ("%g, %g\n", x, y);
#endif
    select_knot (edit, subpath_idx, knot_idx);
    bpath_edit_update (edit);
    status = gnome_canvas_item_grab (
			    edit->bpath,
			    GDK_POINTER_MOTION_MASK | GDK_BUTTON_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			    NULL, 0);
    break;
  case GDK_BUTTON_RELEASE:
    edit->button = FALSE;
    break;
  default:
    return FALSE;
  }
  return TRUE;
}

static int
knot_event (GnomeCanvasItem *item, GdkEvent *event, BpathEdit *edit)
{
  double x, y;
  int n_sel;
  int i;
  int subpath_idx, knot_idx;
  int status;
  gboolean close;
  EditablePath *epath;

  switch (event->type) {
  case GDK_BUTTON_PRESS:
    edit->button = TRUE;
    x = event->button.x;
    y = event->button.y;
    gnome_canvas_item_w2i (item->parent, &x, &y);
    printf ("(%g, %g) %d: ", x, y, event->button.state);
    epath = edit->path;
    closest_knot (epath->path, &subpath_idx, &knot_idx, x, y);
    printf ("knot %d/%d\n", subpath_idx, knot_idx);
    n_sel = 0;
    close = TRUE;
    for (i = 0; i < epath->path->n_subpath; i++)
      {
	if (epath->subpath_sel[i])
	  {
	    n_sel++;
	    if (i != subpath_idx)
	      close = FALSE;
	  }
      }
    if (close && n_sel == 1)
      {
	int j;
	gboolean *ks = epath->knot_sel[subpath_idx];
	int n_knot = epath->path->subpath[subpath_idx].n_knot;

	for (j = 0; j < n_knot; j++)
	  g_print (" %d", ks[j]);
	g_print ("\n");
	if (n_knot < 2 || ks[0] != ks[n_knot - 1])
	  {
	    for (j = 1; j < n_knot - 1; j++)
	      if (ks[j])
		{
		  /* Don't close if any interior knots are selected */
		  close = FALSE;
		  break;
		}
	    if (!((ks[n_knot - 1] && knot_idx == 0) ||
		  (ks[0] && knot_idx == n_knot - 1)))
	      close = FALSE;
	  }
	else
	  /* Don't close if zero or >1 paths selected */
	  close = FALSE;
      }
    if (close)
      {
	printf ("try to close\n");
	close_subpath (edit, subpath_idx);
      }
    select_knot (edit, subpath_idx, knot_idx);
    if (event->button.button == 2)
      toggle_corner (edit, subpath_idx, knot_idx);
    bpath_edit_update (edit);
    status = gnome_canvas_item_grab (
			    edit->bpath,
			    GDK_POINTER_MOTION_MASK | GDK_BUTTON_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			    NULL, 0);
    printf ("grab return = %d\n", status);
    break;
  case GDK_MOTION_NOTIFY:
    if (edit->button)
      {
	Knot *knot;

	x = event->motion.x;
	y = event->motion.y;
	gnome_canvas_item_w2i (item->parent, &x, &y);
	knot = &edit->path->path->subpath[edit->sp_idx].knot[edit->knot_idx];
	knot->x = x;
	knot->y = y;
	bpath_edit_update (edit);
      }
    break;
  case GDK_BUTTON_RELEASE:
    printf ("release\n");
    edit->button = FALSE;
    gnome_canvas_item_ungrab (item, 0);
    break;
  default:
    return FALSE;
  }

  return TRUE;
}

#define RAD 3

static void
bpath_edit_update (BpathEdit *edit)
{
  GnomeCanvasBpathDef *def;
  int n_knot;
  int i, j;
  Subpath *subpath;
  GnomeCanvasItem *knot;
  EditablePath *epath;
  Path *path;
  Path *path2;

  epath = edit->path;
  path = epath->path;

  n_knot = 0;

  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      if (epath->subpath_sel[i])
	n_knot += subpath->n_knot;
    }

  if (edit->n_knot_max < n_knot)
    {
      do
	edit->n_knot_max <<= 1;
      while (edit->n_knot_max < n_knot);
      edit->knot = g_renew (GnomeCanvasItem *, edit->knot, edit->n_knot_max);
    }

  for (i = 0; i < edit->n_knot; i++)
    {
#ifdef VERBOSE
      g_print ("destroy knot %d\n", i);
#endif
      gtk_object_destroy (GTK_OBJECT (edit->knot[i]));
    }

  n_knot = 0;
  for (i = 0; i < path->n_subpath; i++)
    {
      subpath = &path->subpath[i];
      if (epath->subpath_sel[i])
	{
	  for (j = 0; j < subpath->n_knot; j++)
	    {
	      g_print ("making canvas item for knot %d\n", j);
	      if (subpath->knot[j].left_type == KNOT_CURL ||
		  subpath->knot[j].right_type == KNOT_CURL)
		knot =
		  gnome_canvas_item_new (edit->group,
					 gnome_canvas_rect_get_type (),
					 "x1", subpath->knot[j].x - RAD,
					 "y1", subpath->knot[j].y - RAD,
					 "x2", subpath->knot[j].x + RAD,
					 "y2", subpath->knot[j].y + RAD,
					 NULL);
	      else
		knot =
		  gnome_canvas_item_new (edit->group,
					 gnome_canvas_ellipse_get_type (),
					 "x1", subpath->knot[j].x - RAD,
					 "y1", subpath->knot[j].y - RAD,
					 "x2", subpath->knot[j].x + RAD,
					 "y2", subpath->knot[j].y + RAD,
					 NULL);
	      if (epath->knot_sel[i][j])
		gnome_canvas_item_set (knot,
				       "fill_color_rgba", 0x000000ff,
				       NULL);
	      else
		{
		  gnome_canvas_item_set (knot,
					 "fill_color_rgba", 0xffffffff,
					 "outline_color_rgba", 0x000000ff,
					 "width_units", 1.0,
					 NULL);
		}
	      gtk_signal_connect (GTK_OBJECT (knot), "event",
				  (GtkSignalFunc) knot_event, edit);
	      edit->knot[n_knot++] = knot;
	    }
	}
    }
  edit->n_knot = n_knot;


  path2 = path_copy (path);
  make_choices (path2);
  def = convert_path_to_bpath_def (path2);
  path_free (path2);
  gnome_canvas_item_set (edit->bpath,
			 "bpath", def,
			 NULL);
  gnome_canvas_bpath_def_free (def);

}

/**
 * editable_path_from_path: Create a new editable path, from an existing path.
 * @path: The path, or NULL for a new empty editable path.
 *
 * Copies and wraps a path in an editing context. Initially, nothing
 * is selected.
 *
 * Return value: The resulting #EditablePath.
 **/
static EditablePath *
editable_path_from_path (Path *path)
{
  EditablePath *result = g_new (EditablePath, 1);
  Path *new_path = g_new (Path, 1);
  int subpath_idx;
  int n_subpath, n_subpath_max;

  if (path)
    n_subpath = path->n_subpath;
  else
    n_subpath = 0;
  n_subpath_max = n_subpath ? n_subpath : 1;

  new_path->n_subpath = n_subpath;
  new_path->subpath = g_new (Subpath, n_subpath_max);

  result->path = new_path;
  result->n_subpath_max = n_subpath_max;
  result->n_knot_max = g_new (int, n_subpath_max);
  result->subpath_sel = g_new (gboolean, n_subpath_max);
  result->knot_sel = g_new (gboolean *, n_subpath_max);

  for (subpath_idx = 0; subpath_idx < n_subpath; subpath_idx++)
    {
      Subpath *subpath = &path->subpath[subpath_idx];
      Subpath *new_subpath = &new_path->subpath[subpath_idx];
      int knot_idx;
      int n_knot, n_knot_max;

      n_knot = subpath->n_knot;
      n_knot_max = n_knot ? n_knot : 1;

      result->n_knot_max[subpath_idx] = n_knot_max;
      result->subpath_sel[subpath_idx] = FALSE;
      result->knot_sel[subpath_idx] = g_new (gboolean, n_knot_max);
      new_subpath->n_knot = n_knot;
      new_subpath->knot = g_new (Knot, n_knot_max);
      for (knot_idx = 0; knot_idx < n_knot; knot_idx++)
	{
	  result->knot_sel[subpath_idx][knot_idx] = FALSE;

	  /* this copy could be done outside the loop with a memcpy */
	  new_subpath->knot[knot_idx] = subpath->knot[knot_idx];
	}
    }
  return result;
}

static BpathEdit *
bpath_edit_new (GnomeCanvasGroup *group, Path *path)
{
  BpathEdit *result;
  EditablePath *epath;

  result = g_new (BpathEdit, 1);

  result->group = group;
  result->acetate = gnome_canvas_item_new (group,
					   gnome_canvas_acetate_get_type (),
					   NULL);

  result->bpath = gnome_canvas_item_new (group,
					 gnome_canvas_bpath_get_type (),
					 "width_units", 2.0,
					 "outline_color_rgba", 0x000080ff,
					 NULL);
  epath = editable_path_from_path (path);
  result->path = epath;

  result->button = 0;
  result->knot_idx = -1;

  result->n_knot = 0;
  result->n_knot_max = 1;
  result->knot = g_new (GnomeCanvasItem *, result->n_knot_max);

  bpath_edit_update (result);

  gtk_signal_connect (GTK_OBJECT (result->acetate), "event",
		      (GtkSignalFunc) acetate_event, result);

  gtk_signal_connect (GTK_OBJECT (result->bpath), "event",
		      (GtkSignalFunc) knot_event, result);

  return result;
}


static SplineCtx *
new_spline (Path *path)
{
  SplineCtx *spline;
  GnomeCanvasGroup *group;

  spline = g_new (SplineCtx, 1);
  spline->app = gnome_app_new ("spline", "Spline");

  gtk_signal_connect (GTK_OBJECT (spline->app), "delete_event",
		      GTK_SIGNAL_FUNC (quit_cb),
		      NULL);

  spline->canvas = gnome_canvas_new_aa ();
  gtk_widget_set_usize (spline->canvas, 500, 500);

  gtk_widget_show (spline->canvas);

  gnome_canvas_set_scroll_region (GNOME_CANVAS (spline->canvas),
				  0, 0, 500, 500);

  group = gnome_canvas_root (GNOME_CANVAS (spline->canvas));

  spline->edit = bpath_edit_new (group, path);

  gnome_app_set_contents (GNOME_APP (spline->app), spline->canvas);

  gtk_widget_show (spline->app);

  return spline;
}

#define NK 10

int
main (int argc, char **argv)
{
  SplineCtx *spline;
  Knot knot[NK];
  Subpath subpath[1] = { { NK, knot } };
  Path path = { 1, subpath };
  int i;

  gnome_init ("tscroll", "VERSION", argc, argv);

  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
  gtk_widget_set_default_visual (gdk_rgb_get_visual ());

  knot[0].x = 100;
  knot[0].y = 200;

  knot[1].x = 200;
  knot[1].y = 200;

  knot[2].x = 300;
  knot[2].y = 200;

  knot[3].x = 400;
  knot[3].y = 300;

  knot[4].x = 100;
  knot[4].y = 300;

  srand (135);
  for (i = 0; i < NK; i++)
    {
      knot[i].left_type = KNOT_OPEN;
      knot[i].right_type = KNOT_OPEN;
      knot[i].left_info.open.tension = 1.0;
      knot[i].right_info.open.tension = 1.0;
      knot[i].x = 100 + 300.0 * rand () / RAND_MAX;
      knot[i].y = 100 + 300.0 * rand () / RAND_MAX;
    }

#if 0
  knot[0].right_info.open.tension = 10.0;
  knot[1].left_info.open.tension = 10.0;
#endif

  /* make curve open */
  knot[0].left_type = KNOT_ENDPOINT;
  knot[0].right_type = KNOT_CURL;
  knot[NK - 1].left_type = KNOT_CURL;
  knot[NK - 1].right_type = KNOT_ENDPOINT;

  path.n_subpath = 0;

  spline = new_spline (&path);

  bpath_edit_update (spline->edit);

  gtk_main ();
  return 0;
}


