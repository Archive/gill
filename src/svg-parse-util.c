/* svg-parse-util.c

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/


#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h> /* for transform parser */
#include <gnome.h>
#include <libgdome/gdome.h>
#include "svg-parse-util.h"
#include "svg.h"




/*
 * some utility functions used to parse the string 
 * parameters of the svg XML nodes
 */




/**
 * svg_css_parse_length:
 * @str:
 * @fixed:
 *
 * Description: Parse a CSS2 length into a pixel value.
 *
 * Returns: returns the length.
 */

double
svg_css_parse_length (const char *str, gint *fixed)
{
  char *p;
  
  /* 
   *  The supported CSS length unit specifiers are: 
   *  em, ex, px, pt, pc, cm, mm, in, and percentages. 
   */
  
  *fixed = FALSE;

  p = strstr (str, "px");
  if (p != NULL)
    {
      *fixed = TRUE;
      return atof (str);
    }
  p = strstr (str, "in");
  if (p != NULL)
    {
      *fixed = TRUE;
      //return svg->pixels_per_inch * atof (str);
    }
  return atof (str);
}






gboolean
svg_css_param_match (const char *str, const char *param_name)
{
  int i;

  for (i = 0; str[i] != '\0' && str[i] != ':'; i++)
    if (param_name[i] != str[i])
      return FALSE;
  return str[i] == ':' && param_name[i] == '\0';
}

int
svg_css_param_arg_offset (const char *str)
{
  int i;

  for (i = 0; str[i] != '\0' && str[i] != ':'; i++);
  if (str[i] != '\0') i++;
  for (; str[i] == ' '; i++);
  return i;
}

/* Parse a CSS2 color, returning rgb */
guint32
svg_css_parse_color (const char *str)
{
  gint val = 0;
  static GHashTable *colors = NULL;

  /* 
   * todo: handle the rgb (r, g, b) and rgb ( r%, g%, b%), syntax 
   * defined in http://www.w3.org/TR/REC-CSS2/syndata.html#color-units 
   */
#ifdef VERBOSE
  g_print ("color = %s\n", str);
#endif
  if (str[0] == '#')
    {
      int i;
      for (i = 1; str[i]; i++)
	{
	  int hexval;
	  if (str[i] >= '0' && str[i] <= '9')
	    hexval = str[i] - '0';
	  else if (str[i] >= 'A' && str[i] <= 'F')
	    hexval = str[i] - 'A' + 10;
	  else if (str[i] >= 'a' && str[i] <= 'f')
	    hexval = str[i] - 'a' + 10;
	  else
	    break;
	  val = (val << 4) + hexval;
	}
      /* handle #rgb case */
      if (i == 4)
	{
	  val = ((val & 0xf00) << 8) |
	    ((val & 0x0f0) << 4) |
	    (val & 0x00f);
	  val |= val << 4;
	}
#ifdef VERBOSE
      printf ("val = %x\n", val);
#endif
    } 
  else
    {
      GString * string;
      if (!colors)
	{
	  colors = g_hash_table_new (g_str_hash, g_str_equal);
	  
	  g_hash_table_insert (colors, "black",    GINT_TO_POINTER (0x000000));
	  g_hash_table_insert (colors, "silver",   GINT_TO_POINTER (0xc0c0c0));
	  g_hash_table_insert (colors, "gray",     GINT_TO_POINTER (0x808080));
	  g_hash_table_insert (colors, "white",    GINT_TO_POINTER (0xFFFFFF));
	  g_hash_table_insert (colors, "maroon",   GINT_TO_POINTER (0x800000));
	  g_hash_table_insert (colors, "red",      GINT_TO_POINTER (0xFF0000));
	  g_hash_table_insert (colors, "purple",   GINT_TO_POINTER (0x800080));
	  g_hash_table_insert (colors, "fuchsia",  GINT_TO_POINTER (0xFF00FF));
	  g_hash_table_insert (colors, "green",    GINT_TO_POINTER (0x008000));
	  g_hash_table_insert (colors, "lime",     GINT_TO_POINTER (0x00FF00));
	  g_hash_table_insert (colors, "olive",    GINT_TO_POINTER (0x808000));
	  g_hash_table_insert (colors, "yellow",   GINT_TO_POINTER (0xFFFF00));
	  g_hash_table_insert (colors, "navy",     GINT_TO_POINTER (0x000080));
	  g_hash_table_insert (colors, "blue",     GINT_TO_POINTER (0x0000FF));
	  g_hash_table_insert (colors, "teal",     GINT_TO_POINTER (0x008080));
	  g_hash_table_insert (colors, "aqua",     GINT_TO_POINTER (0x00FFFF));
	}

      string = g_string_down (g_string_new (str));


      /* this will default to black on a failed lookup */
      val = GPOINTER_TO_INT (g_hash_table_lookup (colors, string->str)); 
    }

  return val;
}

guint
svg_css_parse_opacity (const char *str)
{
  char *end_ptr;
  double opacity;

  opacity = strtod (str, &end_ptr);

  if (end_ptr[0] == '%')
    opacity *= 0.01;

  return floor (opacity * 255 + 0.5);
}

double
svg_css_parse_fontsize (SVGContext *svg, const char *str)
{
  char *end_ptr;
  double size;

  /* todo: handle absolute-size and relative-size tags and proper units */
  size = strtod (str, &end_ptr);

  if (end_ptr[0] == '%')
    size  = (svg->font_size * size * 0.01);

  return size;
}

/* Parse a CSS2 style argument, setting the SVG context attributes. */
void
svg_parse_style_arg (SVGContext *svg, const char *str)
{
  int arg_off;

  if (svg_css_param_match (str, "opacity"))
    {
      arg_off = svg_css_param_arg_offset (str);
      svg->opacity = svg_css_parse_opacity (str + arg_off);
    }
  else if (svg_css_param_match (str, "fill"))
    {
      arg_off = svg_css_param_arg_offset (str); 
      if (!strcmp (str + arg_off, "none"))
	svg->fill = 0;
      else
	{
	  svg->fill = 1;
	  svg->fill_color = svg_css_parse_color (str + arg_off);
	}
    }
  else if (svg_css_param_match (str, "fill-opacity"))
    {
      arg_off = svg_css_param_arg_offset (str);
      svg->fill_opacity = svg_css_parse_opacity (str + arg_off);
    }
  else if (svg_css_param_match (str, "stroke"))
    {
      arg_off = svg_css_param_arg_offset (str);
      if (!strcmp (str + arg_off, "none"))
	svg->stroke = 0;
      else
	{
	  svg->stroke = 1;
	  svg->stroke_color = svg_css_parse_color (str + arg_off);
	}
    }
  else if (svg_css_param_match (str, "stroke-width"))
    {
      int fixed; 
      arg_off = svg_css_param_arg_offset (str);
      svg->stroke_width = svg_css_parse_length (str + arg_off, &fixed);
    }
  else if (svg_css_param_match (str, "stroke-linecap"))
    {
      arg_off = svg_css_param_arg_offset (str);
      if (!strcmp (str + arg_off, "butt"))
	svg->cap = GDK_CAP_BUTT;
      else if (!strcmp (str + arg_off, "round"))
	svg->cap = GDK_CAP_ROUND;
      else if (!strcmp (str + arg_off, "square"))
	svg->cap = GDK_CAP_PROJECTING;
      else
	g_warning ("unknown line cap style %s", str + arg_off);
    }
  else if (svg_css_param_match (str, "stroke-opacity"))
    {
      arg_off = svg_css_param_arg_offset (str);
      svg->stroke_opacity = svg_css_parse_opacity (str + arg_off);
    }
  else if (svg_css_param_match (str, "stroke-linejoin"))
    {
      arg_off = svg_css_param_arg_offset (str);
      if (!strcmp (str + arg_off, "miter"))
	svg->join = GDK_JOIN_MITER;
      else if (!strcmp (str + arg_off, "round"))
	svg->join = GDK_JOIN_ROUND;
      else if (!strcmp (str + arg_off, "bevel"))
	svg->join = GDK_JOIN_BEVEL;
      else
	g_warning ("unknown line join style %s", str + arg_off);
    }
  else if (svg_css_param_match (str, "font-size"))
    {
      arg_off = svg_css_param_arg_offset (str);
      svg->font_size = svg_css_parse_fontsize (svg, str + arg_off);
    }
  else if (svg_css_param_match (str, "font-family"))
    {
      arg_off = svg_css_param_arg_offset (str);
      svg->font_family = g_strdup (str + arg_off);
    }

}

/* Split a CSS2 style into individual style arguments, setting attributes
   in the SVG context.

   It's known that this is _way_ out of spec. A more complete CSS2
   implementation will happen later.
*/
void
svg_parse_style (SVGContext *svg, const char *str)
{
  int start, end;
  char *arg;

  start = 0;
  while (str[start] != '\0')
    {
      for (end = start; str[end] != '\0' && str[end] != ';'; end++);
      arg = g_new (char, 1 + end - start);
      memcpy (arg, str + start, end - start);
      arg[end - start] = '\0';
      svg_parse_style_arg (svg, arg);
      g_free (arg);
      start = end;
      if (str[start] == ';') start++;
      while (str[start] == ' ') start++;
    }
}

/* Parse an SVG transform string into an affine matrix. Reference: SVG
   working draft dated 1999-07-06, section 8.5. Return TRUE on
   success. */
gboolean
svg_parse_transform (double dst[6], const char *src)
{
  int idx;
  char keyword[32];
  double args[6];
  int n_args;
  int key_len;
  double tmp_affine[6];

  art_affine_identity (dst);

  idx = 0;
  while (src[idx])
    {
      /* skip initial whitespace */
      while (isspace (src[idx]))
	idx++;

      /* parse keyword */
      for (key_len = 0; key_len < sizeof (keyword); key_len++)
	{
	  char c;

	  c = src[idx];
	  if (isalpha (c) || c == '-')
	    keyword[key_len] = src[idx++];
	  else
	    break;
	}
      if (key_len >= sizeof (keyword))
	return FALSE;
      keyword[key_len] = '\0';

      /* skip whitespace */
      while (isspace (src[idx]))
	idx++;

      if (src[idx] != '(')
	return FALSE;
      idx++;

      for (n_args = 0; ; n_args++)
	{
	  char c;
	  char *end_ptr;

	  /* skip whitespace */
	  while (isspace (src[idx]))
	    idx++;
	  c = src[idx];
	  if (isdigit (c) || c == '+' || c == '-' || c == '.')
	    {
	      if (n_args == sizeof(args) / sizeof(args[0]))
		return FALSE; /* too many args */
	      args[n_args] = strtod (src + idx, &end_ptr);
	      idx = end_ptr - src;

	      while (isspace (src[idx]))
		idx++;

	      /* skip optional comma */
	      if (src[idx] == ',')
		idx++;
	    }
	  else if (c == ')')
	    break;
	  else
	    return FALSE;
	}
      idx++;

      /* ok, have parsed keyword and args, now modify the transform */
      if (!strcmp (keyword, "matrix"))
	{
	  if (n_args != 6)
	    return FALSE;
	  art_affine_multiply (dst, args, dst);
	}
      else if (!strcmp (keyword, "translate"))
	{
	  if (n_args == 1)
	    args[1] = 0;
	  else if (n_args != 2)
	    return FALSE;
	  art_affine_translate (tmp_affine, args[0], args[1]);
	  art_affine_multiply (dst, tmp_affine, dst);
	}
      else if (!strcmp (keyword, "scale"))
	{
	  if (n_args == 1)
	    args[1] = args[0];
	  else if (n_args != 2)
	    return FALSE;
	  art_affine_scale (tmp_affine, args[0], args[1]);
	  art_affine_multiply (dst, tmp_affine, dst);
	}
      else if (!strcmp (keyword, "rotate"))
	{
	  if (n_args != 1)
	    return FALSE;
	  art_affine_rotate (tmp_affine, args[0]);
	  art_affine_multiply (dst, tmp_affine, dst);
	}
      else if (!strcmp (keyword, "skewX"))
	{
	  if (n_args != 1)
	    return FALSE;
	  art_affine_shear (tmp_affine, args[0]);
	  art_affine_multiply (dst, tmp_affine, dst);
	}
      else if (!strcmp (keyword, "skewY"))
	{
	  if (n_args != 1)
	    return FALSE;
	  art_affine_shear (tmp_affine, args[0]);
	  /* transpose the affine, given that we know [1] is zero */
	  tmp_affine[1] = tmp_affine[2];
	  tmp_affine[2] = 0;
	  art_affine_multiply (dst, tmp_affine, dst);
	}
      else
	return FALSE; /* unknown keyword */
    }
  return TRUE;
}

/* Parse a coordinate string (i.e. points parameter of polyline), return
   points data structure suitable for Gnome canvas */
GnomeCanvasBpathDef *
svg_parse_points (const char *str)
{
  int i, j;
  char *endptr;
  GnomeCanvasBpathDef *result;
  double value, x = 0.0, y = 0.0;
  double oldx = 0.0, oldy = 0.0;

  result = gnome_canvas_bpath_def_new ();

  j = 0;
  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] != ' ' && str[i] != ',')
	{
	  value = strtod (str + i, &endptr);
	  i = endptr - str - 1;
	  if (!(j & 1))
	    x = value;
	  else
	    {
	      y = value;
	      if (j == 1)
		gnome_canvas_bpath_def_moveto (result, x, y);
	      else if (x != oldx || y != oldy)
		gnome_canvas_bpath_def_lineto (result, x, y);
	      oldx = x;
	      oldy = y;
	    }
	  j++;
	}
    }

  return result;
}
