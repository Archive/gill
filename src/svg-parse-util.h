/* svg-parse-util.h

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/


#ifndef SVG_PARSE_UTIL_H
#define SVG_PARSE_UTIL_H


#include "svg.h"
#include "libgnomeprint/gnome-canvas-bpath.h"

double               svg_css_parse_length        (const char *str, gint *fixed);
gboolean             svg_css_param_match         (const char *str, const char *param_name);
int                  svg_css_param_arg_offset    (const char *str);
guint32              svg_css_parse_color         (const char *str);
guint                svg_css_parse_opacity       (const char *str);
double               svg_css_parse_fontsize      (SVGContext *svg, const char *str);



gboolean             svg_parse_transform         (double dst[6], const char *src);
GnomeCanvasBpathDef *svg_parse_points            (const char *str);
void                 svg_parse_style_arg         (SVGContext *svg, const char *str);
void                 svg_parse_style             (SVGContext *svg, const char *str);


#endif /* SVG_PARSE_UTIL_H */
