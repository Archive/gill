/* svg-parse-util.c

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

*/

/*
 * svg-path-encode.c:
 *
 *   Given a GnomeCanvasBpathDef it generates the SVG path
 *   description.
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 */

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <gnome.h>
#include <libgdome/gdome.h>
#include "libgnomeprint/gnome-canvas-bpath-util.h"
#include "svg.h"
#include "svg-util.h"
#include "svg-path.h"
#include "svg-path-encode.h"

/**
 * svg_encode_path:
 * @path: A GnomeCanvasBpathDef
 *
 * Encodes the GnomeCanvasBpathDev into a string usable
 * in the "d" attribute of a path svg element
 *
 * Returns: A string representing the path
 */
char *
svg_encode_path (GnomeCanvasBpathDef *path)
{
  GString *result;
  int i;
  ArtBpath *bpath;
  int closed = 0;
  char *res;
  
  g_return_val_if_fail (path != NULL, NULL);
  
  bpath = path->bpath;
  result = g_string_sized_new (40);
  
  for (i = 0; i < path->n_bpath; i++){
    switch (bpath [i].code){
    case ART_LINETO:
      g_string_sprintfa (result, "L %g %g ", bpath [i].x3, bpath [i].y3);
      break;
      
    case ART_CURVETO:
      g_string_sprintfa (
			 result, "C %g %g %g %g %g %g ",
			 bpath [i].x1, bpath [i].y1,
			 bpath [i].x2, bpath [i].y2,
			 bpath [i].x3, bpath [i].y3);
      break;
      
    case ART_MOVETO_OPEN:
    case ART_MOVETO:
      if (closed)
	g_string_append  (result, "z ");
      closed = (bpath [i].code == ART_MOVETO);
      g_string_sprintfa (result, "M %g %g ", bpath [i].x3, bpath [i].y3);
      break;
    case ART_END:
      break;
    }
  }
  if (closed)
    g_string_append (result, "z ");
  res = result->str;
  g_string_free (result, FALSE);
  
  return res;
}
