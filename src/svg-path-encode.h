#ifndef GILL_SVG_PATH_ENCODE_H
#define GILL_SVG_PATH_ENCODE_H

char *svg_encode_path (GnomeCanvasBpathDef *path);

#endif /* GILL_SVG_PATH_ENCODE_H */
