/* svg-util.c

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/


#include <gnome.h>
#include <libgdome/gdome.h>
#include "svg-util.h"

static void
svg_mk_const_gdome_str_unref (GdomeDOMString *self)
{
  g_free (self);
}

GdomeDOMString *
svg_mk_const_gdome_str (const char *str)
{
  GdomeDOMString *result;

  result = g_new (GdomeDOMString, 1);
  result->unref = svg_mk_const_gdome_str_unref;
  result->str = str;
  return result;
}

