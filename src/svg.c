/* svg.c

   Copyright (C) 1999,2000 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gill is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with Gill; see the file COPYING. If not, write to the Free
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   Author: Raph Levien <raph@acm.org> */


#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <gnome.h>
#include <tree.h> /* gnome-xml */
#include <libgdome/gdome.h>
#include <libgdome/gdome-xml.h>
#include <libgdome/gdome-util.h>
#include <libart_lgpl/art_affine.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include <libgnomeprint/gnome-font.h>
#include "libgnomeprint/gnome-canvas-bpath.h"
#include "libgnomeprint/gnome-canvas-bpath-util.h"
#include "libgnomeprint/gt1-parset1.h"
#include "libgnomeprint/gnome-canvas-hacktext.h"
#include "svg-parse-util.h"
#include "svg-util.h"
#include "svg-path.h"
#include "svg-path-encode.h"
#include "svg.h"

SVGContext          *svg_context_new             (void);
static void          svg_transform_set           (GdomeNode *node,
						  GnomeCanvasItem *item);
char                *svg_get_string_attribute    (GdomeNode *node,
						  char *name, 
						  char *default_val,
						  int *error_flag);
double               svg_get_numeric_attribute   (GdomeNode *node,
						  char *name, 
						  char *default_val,
						  gint *error_flag);
SVGContext          *svg_style_push              (RenderContextSVG *ctx_svg,
						  GdomeNode *node);
void                 svg_style_pop               (RenderContextSVG *ctx_svg);
double               svg_get_csslength_attribute (gint *fixed, 
						  GdomeNode *node, 
						  char *name, 
						  char *default_val, 
						  int *error_flag);
void                 svg_numeric_attribute_offset (GdomeNode *node, 
						   char *name, 
						   char *default_val, 
						   double offset);


static char* docbase = NULL;

RenderObj *
domination_render (RenderContext *ctx, GnomeCanvasGroup *parent,
		   GdomeNode *node)
{
  RenderFunc *rf;
  int type;
  GdomeException exc = 0;
  GdomeDOMString *tagName;
  RenderObj *result = NULL;

  type = gdome_n_nodeType (node, &exc);
  if (type == GDOME_ELEMENT_NODE)
    {
      tagName = gdome_el_tagName ((GdomeElement *)node, &exc);
      rf = g_hash_table_lookup (ctx->dispatch, tagName->str);
      if (rf == NULL)
	g_warning ("renderer for tag %s not found\n", tagName->str);
      else
	result = (*rf) (ctx, parent, node);
      gdome_str_unref (tagName);
    }
  return result;
}

char *
get_docbase (void)
{
  return docbase;
}

void
set_docbase (char *path)
{
  if (docbase)
    g_free (docbase);

  docbase = g_strdup (path);
}

static void
svg_primitive_delete (RenderObjEditable *roe)
{
	GdomeNode *parent;
	GdomeException exc = 0;
	
	printf ("trying to delete\n");

	parent = gdome_n_parentNode (roe->node, &exc);
	gdome_n_removeChild ((GdomeNode *)parent, (GdomeNode *) roe->node, &exc);
	gdome_n_unref (roe->node, &exc);
	gtk_object_destroy (GTK_OBJECT (roe->ro.item));
	g_free (roe);
}

static int
item_event (GnomeCanvasItem *item, GdkEvent *event, RenderObjEditable *roe)
{
	double x, y;  
	GdkEventKey *kevent;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		roe->button_pressed = TRUE;

		x = event->button.x;
		y = event->button.y;
		gnome_canvas_item_w2i (item->parent, &x, &y);
		roe->button_x = x;
		roe->button_y = y;
		/* The item grab is probably turned off because if Gill crashes
		   in the middle of the grab, it will cause X to keep the
		   pointer grabbed. It should be turned on once Gill
		   attains Bug Free status. */
#if 0
                gnome_canvas_item_grab (
			item,
			GDK_POINTER_MOTION_MASK | GDK_BUTTON_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			NULL, 0);
#endif
		break;

	case GDK_MOTION_NOTIFY:
		if (roe->button_pressed){
			x = event->motion.x;
			y = event->motion.y;
			gnome_canvas_item_w2i (item->parent, &x, &y);
			if (roe->move)
				(*roe->move)(roe, x - roe->button_x, y - roe->button_y);
			roe->button_x = x;
			roe->button_y = y;
		} else {
		  printf ("moved over item = %p\n", item);
		}
		break;

	case GDK_BUTTON_RELEASE:
		roe->button_pressed = FALSE;
		gnome_canvas_item_ungrab (item, 0);
		break;
	case GDK_KEY_PRESS:
		kevent = (GdkEventKey *) event;

		x = y = 0.0;
		switch (kevent->keyval)
		{
		case GDK_Left: 
			x = -1.0;
			break;
		case GDK_Right:
			x = 1.0;
			break;
		case GDK_Up: 	
			y = -1.0;
			break;
		case GDK_Down:
			y = 1.0;
			break;
		case GDK_Delete:
		case GDK_BackSpace:
			svg_primitive_delete (roe);
			roe = NULL;
			return (TRUE);
			break;
#if 0
		case ' ':
		  g_print ("space pressed!\n");
		  return (TRUE);
		default:
		  g_print ("keyval = %d\n", kevent->keyval);
		  return (FALSE);
#endif
		}

		if (roe->move)
			(*roe->move)(roe, x, y);

		printf ("item = %p\n", item);
	        break;
	default:
		return FALSE;
	}
	
	return TRUE;
}

static void
canvas_make_editable (RenderObjEditable *roe)
{
	gtk_signal_connect (
		GTK_OBJECT (roe->ro.item), "event",
		GTK_SIGNAL_FUNC (item_event), roe);

}


static SVGContext *
svg_context_clone (SVGContext *svg)
{
  SVGContext *result = g_new (SVGContext, 1);

  memcpy (result, svg, sizeof (SVGContext));
  return result;
}

static void
svg_context_free (SVGContext *svg)
{
  g_free (svg);
}


static void
svg_transform_set (GdomeNode *node, GnomeCanvasItem *item)
{ 
  double affine[6];
  char *transform;
  int error_flag;
  
  transform = svg_get_string_attribute (node, "transform", "", &error_flag);
  svg_parse_transform (affine, transform);
  gnome_canvas_item_affine_relative (item, affine);
  
  g_free (transform);
}

SVGContext *
svg_style_push (RenderContextSVG *ctx_svg, GdomeNode *node)
{
  GdomeException exc = 0;  
  GdomeDOMString *style;
  GdomeDOMString *string;
  
  style = gdome_el_getAttribute ((GdomeElement *) node, string = svg_mk_const_gdome_str ("style"),
				 &exc);
  gdome_str_unref (string);

  if (style != NULL)
    {
      /* clone svg so style only affects children */
      ctx_svg->svg = svg_context_clone (ctx_svg->svg);

      svg_parse_style (ctx_svg->svg, style->str);
      gdome_str_unref (style);

      ctx_svg->context_stack = g_slist_prepend (ctx_svg->context_stack, ctx_svg->svg);
      return ctx_svg->svg;
    }
  
  ctx_svg->context_stack = g_slist_prepend (ctx_svg->context_stack, NULL);
  return NULL;

}

void
svg_style_pop (RenderContextSVG *ctx_svg)
{
  GSList *item;

  item = ctx_svg->context_stack;
  ctx_svg->context_stack = g_slist_next (item);

  if (item->data)
    {
      GSList *list = ctx_svg->context_stack;

      while (list && list->data == NULL)
	list = g_slist_next (list);

      if (list) {
	ctx_svg->svg = (SVGContext *)list->data;
	svg_context_free ((SVGContext *)item->data);
      } else {
	g_warning ("There appears to be an error in the style stack handling code");
      }
    }

  g_slist_free_1 (item);
}

static RenderObj *
svg_render_svg (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  /*  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;*/
  RenderObj *result = g_new (RenderObj, 1);
  GdomeException exc = 0;
  GdomeNode *child, *next;
  RenderObj *child_ro;

  g_print ("rendering SVG node\n");

  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      child_ro = domination_render (ctx, parent, child);
      next = gdome_n_nextSibling (child, &exc);
/*      gdome_n_unref (child, &exc); */
    }

  result->item = NULL;

  return result;
}

static RenderObj *
svg_render_g (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObj *result = g_new (RenderObj, 1);
  GnomeCanvasGroup *group;
  GdomeException exc = 0;
  GdomeNode *child, *next;
  RenderObj *child_ro;

  svg_style_push (ctx_svg, node);

  group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (parent,
						     gnome_canvas_group_get_type (),
						     "x", 0.0,
						     "y", 0.0,
						     NULL));

  result->item = GNOME_CANVAS_ITEM (group);
  svg_transform_set (node, result->item);

  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      child_ro = domination_render (ctx, group, child);
      next = gdome_n_nextSibling (child, &exc);
/*      gdome_n_unref (child, &exc); */
    }

  /* restore original svg */

  svg_style_pop (ctx_svg);

  return result;
}

static RenderObj *
svg_render_defs (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  int error_flag = 0;
  GdomeException exc = 0;
  GdomeNode *child, *next;
  int type;
  char *id;

  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      type = gdome_n_nodeType (node, &exc);
      
      if (type == GDOME_ELEMENT_NODE)
	{
	  id = svg_get_string_attribute (child, "id", NULL, &error_flag);
	  if (id)
	    g_hash_table_insert (ctx_svg->defs, id, child);

	}
      next = gdome_n_nextSibling (child, &exc);
      /*    gdome_n_unref (child, &exc); */
    }
  
  return NULL;
}
static RenderObj *
svg_render_use (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObj *result = g_new (RenderObj, 1);
  GnomeCanvasGroup *group;
  /*  GdomeException exc = 0; */
  GdomeNode *child /*, *next*/ ;
  RenderObj *child_ro;
  int error_flag = 0;
  char *href;

  svg_style_push (ctx_svg, node);

  group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (parent,
						     gnome_canvas_group_get_type (),
						     "x", 0.0,
						     "y", 0.0,
						     NULL));

  result->item = GNOME_CANVAS_ITEM (group);
  svg_transform_set (node, result->item);

  href = svg_get_string_attribute (node, "href", NULL, &error_flag);
  
  if (href) 
    {
      child = g_hash_table_lookup (ctx_svg->defs, href);
      
      if (child)
	child_ro = domination_render (ctx, group, child);
      else 
	g_warning ("svg_render_use: id not found");
   }
  /* restore original svg */

  svg_style_pop (ctx_svg);

  return result;
}

#define EPS 1e-6

#if 0

/* get rid of duplicate points */
static GnomeCanvasPoints *
clean_points (GnomeCanvasPoints *points)
{
  GnomeCanvasPoints *result;
  int i, n_unique, j;

  n_unique = 1;
  for (i = 1; i < points->num_points; i++)
    if (fabs (points->coords[i * 2] - points->coords[i * 2 - 2]) > EPS ||
	fabs (points->coords[i * 2 + 1] - points->coords[i * 2 - 1]) > EPS)
      n_unique++;

  result = gnome_canvas_points_new (n_unique - 1);
  result->coords[0] = points->coords[0];
  result->coords[1] = points->coords[1];
  j = 1;
  for (i = 1; j < n_unique - 1 && i < points->num_points; i++)
    if (fabs (points->coords[i * 2] - points->coords[i * 2 - 2]) > EPS ||
	fabs (points->coords[i * 2 + 1] - points->coords[i * 2 - 1]) > EPS)
      {
	result->coords[j * 2] = points->coords[i * 2];
	result->coords[j * 2 + 1] = points->coords[i * 2 + 1];
	j++;
      }

  gnome_canvas_points_unref (points);

  return result;
}

static double
compute_area (GnomeCanvasPoints *points)
{
  double area = 0.0;
  int n_points = points->num_points;
  int i;

  for (i = 0; i < n_points; i++)
    {
      int j = (i + 1) % n_points;
      area += points->coords[j * 2] * points->coords[i * 2 + 1] -
	points->coords[i * 2] * points->coords[j * 2 + 1];
    }
  return area * 0.5;
}

static GnomeCanvasPoints *
reverse_points (GnomeCanvasPoints *points)
{
  GnomeCanvasPoints *result;
  int i;

  result = gnome_canvas_points_new (points->num_points);
  for (i = 0; i < points->num_points; i++)
    {
      int j = points->num_points - 1 - i;
      result->coords[j * 2] = points->coords[i * 2];
      result->coords[j * 2 + 1] = points->coords[i * 2 + 1];
    }

  gnome_canvas_points_unref (points);

  return result;
}
#endif

static void
set_gdk_color_from_rgb (GdkColor *color, guint32 rgb)
{
  int r, g, b;

  r = (rgb >> 16) & 0xff;
  color->red = r | (r << 8);
  g = (rgb >> 8) & 0xff;
  color->green = g | (g << 8);
  b = (rgb >> 0) & 0xff;
  color->blue = b | (b << 8);
  color->pixel = gdk_rgb_xpixel_from_rgb (rgb);
}

static GnomeCanvasItem *
svg_render_bpath (RenderContext *ctx, GnomeCanvasGroup *parent, GnomeCanvasBpathDef *bpath)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  SVGContext *svg = ctx_svg->svg;
  GnomeCanvasItem *item;
  gint fill_opacity, stroke_opacity;

  fill_opacity = (svg->opacity * svg->fill_opacity) / 255; 
  stroke_opacity = (svg->opacity * svg->stroke_opacity) / 255; 

  item = gnome_canvas_item_new (parent,
				gnome_canvas_bpath_get_type (),
				"bpath", bpath,
				"width_units", svg->stroke_width,
				NULL);

  if (svg->fill)
    gnome_canvas_item_set (item, "fill_color_rgba", (svg->fill_color << 8) |
			   fill_opacity, NULL);

  if (svg->stroke)
    {
      gnome_canvas_item_set (item,
			     "outline_color_rgba", (svg->stroke_color << 8) |
			     stroke_opacity,
			     "cap_style", svg->cap,
			     "join_style", svg->join,
			     NULL);
    }

  return item;
}

char *
svg_get_string_attribute (GdomeNode *node, char *name, char *default_val, int *error_flag)
{
  GdomeDOMString *val_s;
  GdomeException exc = 0;
  char *result;
  GdomeDOMString *string;

  val_s = gdome_el_getAttribute ((GdomeElement *)node, string = svg_mk_const_gdome_str (name), &exc);
  gdome_str_unref(string);

  if (val_s)
    {
      result = g_strdup (val_s->str);
      gdome_str_unref (val_s);
      return result;
    }
  else if (default_val)
    {
      return g_strdup (default_val);
    }

#ifdef VERBOSE
  g_warning ("Missing required string attribute \"%s\"", name);
#endif

  if (error_flag)
    *error_flag |= TRUE;

  return NULL;
}

double
svg_get_csslength_attribute (gint *fixed, GdomeNode *node, char *name, char *default_val, int *error_flag)
{
  char *str;
  
  str = svg_get_string_attribute (node, name, default_val, error_flag);
  return (svg_css_parse_length (str, fixed));
  
}

double
svg_get_numeric_attribute (GdomeNode *node, char *name, char *default_val, gint *error_flag)
{
  GdomeDOMString *val_s;
  GdomeException exc = 0;
  double result;
  GdomeDOMString *string;


  val_s = gdome_el_getAttribute ((GdomeElement *)node, string = svg_mk_const_gdome_str (name), &exc);
  gdome_str_unref(string);

  if (val_s)
    {
      result = atof (val_s->str);
      gdome_str_unref (val_s);
      return result;
    }
  else if (default_val)
    {
      result = atof (default_val);
      return result;
    }

#ifdef VERBOSE
  g_warning ("Missing required numeric attribute \"%s\"", name);
#endif

  if (error_flag)
    *error_flag |= TRUE;

  return 0.0;
}

void
svg_numeric_attribute_offset (GdomeNode *node, char *name, char *default_val, double offset)
{
  GdomeDOMString *name_s;
  GdomeDOMString *value_s;
  double value;
  GdomeException exc = 0;
  char *s;
  int error_flag;

  name_s = svg_mk_const_gdome_str (name);
  value = svg_get_numeric_attribute(node, name, default_val, &error_flag);

  value += offset;

#ifdef VERBOSE
  printf ("%s: %lf", name, value);
#endif

  s = g_strdup_printf ("%g", value);
  value_s = svg_mk_const_gdome_str (s);
  name_s = svg_mk_const_gdome_str (name);
  gdome_el_setAttribute ((GdomeElement *)node, name_s, value_s, &exc);
  g_free (s);
}

static void
svg_xy_move (RenderObjEditable *roe, double dx, double dy)
{
	printf ("%s\n", __PRETTY_FUNCTION__);
	svg_numeric_attribute_offset (roe->node, "x", "0.0", dx);
	svg_numeric_attribute_offset (roe->node, "y", "0.0", dy);

	gnome_canvas_item_move (roe->ro.item, dx, dy);
}

static void
svg_center_move (RenderObjEditable *roe, double dx, double dy)
{
	printf ("%s\n", __PRETTY_FUNCTION__);
	svg_numeric_attribute_offset (roe->node, "cx", "0.0", dx);
	svg_numeric_attribute_offset (roe->node, "cy", "0.0", dy);

	gnome_canvas_item_move (roe->ro.item, dx, dy);
}


static void
svg_line_move (RenderObjEditable *roe, double dx, double dy)
{
	printf ("%s\n", __PRETTY_FUNCTION__);
	svg_numeric_attribute_offset (roe->node, "x1", "0.0", dx);
	svg_numeric_attribute_offset (roe->node, "y1", "0.0", dy);
	svg_numeric_attribute_offset (roe->node, "x2", "0.0", dx);
	svg_numeric_attribute_offset (roe->node, "y2", "0.0", dy);

	gnome_canvas_item_move (roe->ro.item, dx, dy); 
}



/* this callback updates the coordinates of the Canvas item
   associated to the GdomeNode.
   Then, once a redraw occurs, these new coordinates are
   taken into account.
*/
void
poly_cb (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc);


void
poly_cb (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc)
{
  GdomeEventListener *priv = (GdomeEventListener *)self;
  GdomeNode *node;
  GnomeCanvasBpathDef *bpath;
  char *points;
  int error_flag;
  GnomeCanvasItem *private_item = GNOME_CANVAS_ITEM (gdome_xml_evntl_get_priv (GDOME_EVNTL (self)));

  g_print ("poly: got event!\n");

  node = gdome_evnt_target (event, exc);

  points = svg_get_string_attribute (node, "points", NULL, &error_flag);

  bpath = svg_parse_points (points);

  gnome_canvas_bpath_def_closepath (bpath);

  gnome_canvas_item_set (private_item,
			 "bpath", bpath,
			 NULL);

  gnome_canvas_bpath_def_unref (bpath);
}


static void
svg_poly_move (RenderObjEditable *roe, double dx, double dy)
{
	GdomeException exc = 0;
	GnomeCanvasBpathDef *bpd;
	ArtBpath *bpath;
	GString *buffer;
	int i;

	/*
	 * Fetch the ArtBpath
	 */
	bpd = gnome_canvas_bpath_get_def (GNOME_CANVAS_BPATH (roe->ro.item));
	bpath = bpd->bpath;

	/*
	 * Translate it
	 */
	buffer = g_string_new (NULL);
	for (i=0; i < bpd->n_bpath; i++)
	  {
	    switch (bpath [i].code)
	      {
	      case ART_LINETO:
	      case ART_MOVETO_OPEN:
	      case ART_MOVETO:
		g_string_sprintfa (buffer, " %g,%g ", 
				   bpath [i].x3 + dx,
				   bpath [i].y3 + dy);
		printf ("(%g, %g) ==> (%g, %g)\n",
			bpath[i].x3, bpath[i].y3,
			bpath[i].x3 + dx, bpath[i].y3 + dy);
		break;
		
	      default:
		g_warning ("Unkown code in points attribute");
		break;
	      }
	  }

#if 0
	gnome_canvas_item_move (roe->ro.item, dx, dy);
#endif
        gdome_el_setAttribute ((GdomeElement *)roe->node,
			       svg_mk_const_gdome_str ("points"),
			       svg_mk_const_gdome_str (buffer->str),
			       &exc);
	gnome_canvas_bpath_def_unref (bpd);
	g_string_free (buffer, TRUE);
}

static RenderObj *
svg_render_polyline (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  gint error_flag = 0;
  char *points;

  printf ("%s\n", __PRETTY_FUNCTION__);
  points = svg_get_string_attribute (node, "points", NULL, &error_flag);

  if (error_flag)
    return NULL;

  svg_style_push (ctx_svg, node);

  bpath = svg_parse_points (points);

  g_free (points);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);

  result->move = svg_poly_move;  
  result->node = node;
  canvas_make_editable (result);

  gnome_canvas_bpath_def_unref (bpath);
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

static RenderObj *
svg_render_polygon (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GdomeException exc = 0;
  GnomeCanvasBpathDef *bpath;
  gint error_flag = 0;
  GdomeEventListener *listener;
  char *points;

  points = svg_get_string_attribute (node, "points", NULL, &error_flag);

  if (error_flag)
    return NULL;

  svg_style_push (ctx_svg, node);

  bpath = svg_parse_points (points);

  g_free (points);

  gnome_canvas_bpath_def_closepath (bpath);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);

  gnome_canvas_bpath_def_unref (bpath);

  /*
   * Add a listener for DOM updates to polygons.
   */
  listener = gdome_xml_evntl_new (poly_cb, result->ro.item);
  gdome_evntt_addEventListener (GDOME_EVNTT (node),
				svg_mk_const_gdome_str ("attrModified"),
				GDOME_EVNTL (listener), FALSE, &exc);

  
  result->move = svg_poly_move;
  result->node = node;
  canvas_make_editable (result);

  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

#define C1 0.552
static RenderObj *
svg_render_circle (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  double cx, cy, r;
  gint error_flag = 0;

  cx = svg_get_numeric_attribute(node, "cx", "0.0", &error_flag);
  cy = svg_get_numeric_attribute(node, "cy", "0.0", &error_flag);

  r = svg_get_numeric_attribute(node, "r", NULL, &error_flag);

  if (error_flag)
    return NULL;

  svg_style_push (ctx_svg, node);

  bpath = gnome_canvas_bpath_def_new ();
  gnome_canvas_bpath_def_moveto (bpath, cx + r, cy);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx + r, cy - C1 * r,
				  cx + C1 * r, cy - r,
				  cx, cy - r);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx - C1 * r, cy - r,
				  cx - r, cy - C1 * r,
				  cx - r, cy);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx - r, cy + C1 * r,
				  cx - C1 * r, cy + r,
				  cx, cy + r);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx + C1 * r, cy + r,
				  cx + r, cy + C1 * r,
				  cx + r, cy);
  gnome_canvas_bpath_def_closepath (bpath);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);
  result->move = svg_center_move;
  result->node = node;
  canvas_make_editable (result);

  gnome_canvas_bpath_def_unref (bpath);
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

static RenderObj *
svg_render_ellipse (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  double cx, cy, rx, ry;
  gint error_flag = 0;

  cx = svg_get_numeric_attribute(node, "cx", "0.0", &error_flag);
  cy = svg_get_numeric_attribute(node, "cy", "0.0", &error_flag);

  rx = svg_get_numeric_attribute(node, "rx", NULL, &error_flag);
  ry = svg_get_numeric_attribute(node, "ry", NULL, &error_flag);

  if (error_flag)
    return (RenderObj *) NULL;

  svg_style_push (ctx_svg, node);

  bpath = gnome_canvas_bpath_def_new ();
  gnome_canvas_bpath_def_moveto (bpath, cx + rx, cy);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx + rx, cy - C1 * ry,
				  cx + C1 * rx, cy - ry,
				  cx, cy - ry);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx - C1 * rx, cy - ry,
				  cx - rx, cy - C1 * ry,
				  cx - rx, cy);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx - rx, cy + C1 * ry,
				  cx - C1 * rx, cy + ry,
				  cx, cy + ry);
  gnome_canvas_bpath_def_curveto (bpath,
				  cx + C1 * rx, cy + ry,
				  cx + rx, cy + C1 * ry,
				  cx + rx, cy);
  gnome_canvas_bpath_def_closepath (bpath);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);
  result->move = svg_center_move;
  result->node = node;
  canvas_make_editable (result);

  gnome_canvas_bpath_def_unref (bpath);
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

static RenderObj *
svg_render_rect (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  double x, y, rx, ry, width, height;
  gint error_flag = 0;

  x = svg_get_numeric_attribute(node, "x", "0.0", &error_flag);
  y = svg_get_numeric_attribute(node, "y", "0.0", &error_flag);

  width = svg_get_numeric_attribute(node, "width", NULL, &error_flag);
  height = svg_get_numeric_attribute(node, "height", NULL, &error_flag);

  rx = svg_get_numeric_attribute(node, "rx", "0.0", &error_flag);
  ry = svg_get_numeric_attribute(node, "ry", "0.0", &error_flag);

  if (error_flag)
    return (RenderObj *) NULL;

  svg_style_push (ctx_svg, node);

  bpath = gnome_canvas_bpath_def_new ();
  gnome_canvas_bpath_def_moveto (bpath, x + rx, y);
  gnome_canvas_bpath_def_lineto (bpath, x + width - rx, y);
  gnome_canvas_bpath_def_curveto (bpath,
				 x + width - (C1 * rx), y,
				 x + width, y + (C1 * ry),
				 x + width, y + ry);
  gnome_canvas_bpath_def_lineto (bpath, x + width, y + height - ry);
  gnome_canvas_bpath_def_curveto (bpath,
				 x + width, y + height - (C1 * ry),
				 x + width - (C1 * rx), y + height,
				 x + width - rx, y + height);
  gnome_canvas_bpath_def_lineto (bpath, x + rx, y + height);
  gnome_canvas_bpath_def_curveto (bpath,
				 x + (C1 * rx), y + height,
				 x, y + height - (C1 * ry),
				 x, y + height - ry);
  gnome_canvas_bpath_def_lineto (bpath, x, y + ry);
  gnome_canvas_bpath_def_curveto (bpath,
				 x, y + (C1 * ry),
				 x + (C1 * rx), y,
				 x + rx, y);

  gnome_canvas_bpath_def_closepath (bpath);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);
  result->move = svg_xy_move;
  result->node = node;
  canvas_make_editable (result);

  gnome_canvas_bpath_def_unref (bpath);
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

static RenderObj *
svg_render_line (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  double x1, y1, x2, y2;
  gint error_flag = 0;

  x1 = svg_get_numeric_attribute(node, "x1", "0.0", &error_flag);
  y1 = svg_get_numeric_attribute(node, "y1", "0.0", &error_flag);

  x2 = svg_get_numeric_attribute(node, "x2", "0.0", &error_flag);
  y2 = svg_get_numeric_attribute(node, "y2", "0.0", &error_flag);

  if (error_flag)
    return (RenderObj *) NULL;

  svg_style_push (ctx_svg, node);

  bpath = gnome_canvas_bpath_def_new ();
  gnome_canvas_bpath_def_moveto (bpath, x1, y1);
  gnome_canvas_bpath_def_lineto (bpath, x2, y2);

  result->ro.item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, result->ro.item);
  result->move = svg_line_move;
  result->node = node;
  canvas_make_editable (result);

  gnome_canvas_bpath_def_unref (bpath);
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

static RenderObj *
svg_render_image (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  double x, y, width, height;
  int x_pixels, y_pixels, w_pixels, h_pixels;
  gint error_flag = 0;
  GdkPixbuf *pixbuf;
  GnomeCanvasItem *image;
  char *file, *path, tmp[30];

  file = svg_get_string_attribute(node, "href", "notfound.png", &error_flag);


  x = svg_get_csslength_attribute(&x_pixels, node, "x", "0.0", &error_flag);
  y = svg_get_csslength_attribute(&y_pixels, node, "y", "0.0", &error_flag);

  if (error_flag)
    return (RenderObj *) NULL;

  
#ifdef VERBOSE
  printf ("image path = %s, file = %s\n", path);
#endif

  path = get_docbase ();
  pixbuf = gdk_pixbuf_new_from_file (path = g_strconcat (path, "/", file, NULL));

  g_free (path);
  g_free (file);

  if (!pixbuf)
    return (RenderObj *) NULL;

  /* todo: fix this silliness of printing out the string then converting it back */
  g_snprintf (tmp, 30, "%g", (double)gdk_pixbuf_get_width (pixbuf));
  width = svg_get_csslength_attribute(&w_pixels, node, "width", tmp, &error_flag);

  g_snprintf (tmp, 30, "%g", (double)gdk_pixbuf_get_height (pixbuf));
  height = svg_get_csslength_attribute(&h_pixels, node, "height", tmp, &error_flag);
      
  svg_style_push (ctx_svg, node);

  printf ("x = %f, y = %f\n", x, y);

  image = gnome_canvas_item_new (parent,
				 gnome_canvas_pixbuf_get_type (),
				 "pixbuf", pixbuf,

				 "x", x,
				 "y", y,
				 "x_in_pixels", x_pixels,
				 "y_in_pixels", y_pixels, 
				 "width", width,
				 "height", height,
				 "width_set", TRUE,
				 "height_set", TRUE,
				 "width_in_pixels", w_pixels,
				 "height_in_pixels", h_pixels,

				 NULL);

  svg_transform_set (node, image);

  result->ro.item = image;
  result->node = node;
  result->move = svg_xy_move;
  canvas_make_editable (result);
  
  /*  
  gtk_signal_connect (GTK_OBJECT (image), "destroy",
		      (GtkSignalFunc) svg_destroy_image,
		      pixbuf);
  */    
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

struct _FontHack {
  char *path;
  Gt1LoadedFont *lf;
};

typedef struct _FontHack FontHack;

FontHack             *font_hack_new               (char *path);
Gt1LoadedFont        *fonthack_closest            (SVGContext* svg);
static Gt1LoadedFont *fonthack_fetch              (char *pfb_file, char *afm_file);


FontHack *
font_hack_new (char *path)
{
  FontHack *fresh;

  fresh = g_new0 (FontHack, 1);
  fresh->path = path;

  return fresh;
}

Gt1LoadedFont *
fonthack_closest (SVGContext* svg)
{
  GnomeFont *font;
  Gt1LoadedFont *lf = NULL;
  static GHashTable *name_hash = NULL;
  char *font_family;
  static Gt1LoadedFont *default_lf = NULL;

  if (!name_hash){
    name_hash = g_hash_table_new (g_str_hash, g_str_equal);
    g_hash_table_insert (name_hash, "serif", "Times");
    g_hash_table_insert (name_hash, "sans-serif", "Helvetica");
    g_hash_table_insert (name_hash, "monospace", "Courier");
    g_hash_table_insert (name_hash, "cursive", "Zapf-Chancery");
    g_hash_table_insert (name_hash, "fantasy", "Critter");
  }

  font_family = g_hash_table_lookup (name_hash, svg->font_family);
  if (!font_family)
    font_family = svg->font_family;

  font = gnome_font_new_closest (font_family, GNOME_FONT_MEDIUM, FALSE, svg->font_size);

  if (font == NULL)
    {
      g_warning (
		 "Since matching font cannot be found, it would appear that fonts have\n"
		 "not been correctly installed.");
    }  
  else 
    { 
      /* set the default font if something has gone wrong */
  
      lf = fonthack_fetch (font->fontmap_entry->pfb_fn,
			   font->fontmap_entry->afm_fn);
    }

  if (lf == NULL) {
    if (default_lf == NULL)
      default_lf = gt1_load_font ("/usr/X11R6/lib/X11/fonts/Type1/c0582bt_.pfb");
    
    lf = default_lf;
  }

  return lf;
}	

static Gt1LoadedFont *
fonthack_fetch (char *pfb_file, char *afm_file)
{
	Gt1LoadedFont *font;
	static GHashTable *font_hash = NULL;

	if (!font_hash){
		font_hash = g_hash_table_new (g_str_hash, g_str_equal);
	}

	font = g_hash_table_lookup (font_hash, pfb_file);
	if (font == (void *) -1)
		return NULL;

	if (font)
		return font;

	if (font == NULL){
		font = gt1_load_font (pfb_file);
		if (font == NULL)
			return NULL;

		g_hash_table_insert (font_hash, g_strdup (pfb_file), font);
	}
	return font;
}

static RenderObj *
svg_render_text (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  SVGContext *svg;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  double x, y;
  gint error_flag = 0;
  GnomeCanvasItem *hacktext;
  GdomeDOMString *val_s;
  GdomeException exc = 0;
  char *text;
  GdomeNode *child, *next;
  Gt1LoadedFont *lf = NULL;

  text = "";

  x = svg_get_numeric_attribute(node, "x", "0.0", &error_flag);
  y = svg_get_numeric_attribute(node, "y", "0.0", &error_flag);
  
  svg_style_push (ctx_svg, node);
  svg = ctx_svg->svg;

  lf = fonthack_closest (svg);

  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      if ((val_s = gdome_n_nodeValue (child, &exc)))
	text = val_s->str;

      next = gdome_n_nextSibling (child, &exc);
      gdome_str_unref (val_s);
    }
  
  if (error_flag)
    return (RenderObj *) NULL;
  
  printf ("x = %f, y = %f\n", x, y);
  
  hacktext = gnome_canvas_item_new (parent,
				    gnome_canvas_hacktext_get_type (),
				    "font", lf,
				    "size", svg->font_size,
				    "x", x,
				    "y", y,
				    "text", text,
				    NULL);
  
  
  svg_transform_set (node, hacktext);

  if (svg->fill)
    gnome_canvas_item_set (hacktext, "fill_color_rgba", 
			   (svg->fill_color << 8) |
			   (svg->opacity * svg->fill_opacity/255), NULL);
  
  if (svg->stroke)
    gnome_canvas_item_set (hacktext,
			   "outline_color_rgba",
			   (svg->stroke_color << 8) |
			   (svg->opacity * svg->stroke_opacity/255),
			   "cap_style", svg->cap,
			   "join_style", svg->join,
			   "width_units", svg->stroke_width,
			   NULL);

  result->ro.item = hacktext;
  result->node = node;
  result->move = svg_xy_move;
  canvas_make_editable (result);
  
  /*  
  gtk_signal_connect (GTK_OBJECT (image), "destroy",
		      (GtkSignalFunc) svg_destroy_text,
		      pixbuf);
  */    
  svg_style_pop (ctx_svg);

  return (RenderObj *) result;
}

typedef struct _GdomeEventListenerPath GdomeEventListenerPath;

struct _GdomeEventListenerPath {
  GdomeEventListener l;

  GnomeCanvasItem *item;
};

static void
svg_path_listener_unref (GdomeEventListener *self, GdomeException *exc)
{
}

static void
svg_path_listener_handleEvent (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc)
{
  GdomeEventListenerPath *priv = (GdomeEventListenerPath *)self;
  GdomeNode *node;
  GnomeCanvasBpathDef *bpath;

  g_print ("path: got event!\n");

  /* Be don't have to get the node, we could parse the attribute and new
     value out of the event. But this would require a mild API change. */
  node = gdome_evnt_target (event, exc);

  bpath = svg_parse_path (node);
  gnome_canvas_item_set (priv->item,
			 "bpath", bpath,
			 NULL);

  gnome_canvas_bpath_def_unref (bpath);

#if 0
  /* todo: this should be done, but the evnt_target impl is not ref'ing */
  gdome_n_unref (node, &exc);
#endif
}

const GdomeEventListenerVtab svg_path_listener_vtab = {
#ifdef GDOME_REFCOUNT
  NULL,
#endif
  svg_path_listener_unref,
#ifdef GDOME_REFCOUNT
  NULL,
#endif
  svg_path_listener_handleEvent
};

static void
svg_path_move (RenderObjEditable *roe, double dx, double dy)
{
	GdomeDOMString *aname;
	GdomeDOMString *value;
	GdomeException exc = 0;
	GnomeCanvasBpathDef *bpd;
	ArtBpath *bpath;
	/*	ArtPoint p;*/
	int i;
	char *s;

	printf ("%s\n", __PRETTY_FUNCTION__);
	aname = svg_mk_const_gdome_str ("d");
	
	/*
	 * Fetch the ArtBpath
	 */
	bpd = gnome_canvas_bpath_get_def (GNOME_CANVAS_BPATH (roe->ro.item));
	bpath = bpd->bpath;

	/*
	 * Translate it
	 */
	for (i = 0; i < bpd->n_bpath; i++){
		switch (bpath [i].code){
		case ART_LINETO:
		case ART_MOVETO_OPEN:
		case ART_MOVETO:
			bpath [i].x3 += dx;
			bpath [i].y3 += dy;
			break;

		case ART_CURVETO:
			bpath [i].x1 += dx;
			bpath [i].x2 += dx;
			bpath [i].x3 += dx;
			bpath [i].y1 += dy;
			bpath [i].y2 += dy;
			bpath [i].y3 += dy;
			break;

		default:
		}
	}
	s = svg_encode_path (bpd);
#ifdef VERBOSE
	printf ("Code: %s\n", s);
#endif
	value = svg_mk_const_gdome_str (s);
	gnome_canvas_bpath_def_unref (bpd);
	gdome_el_setAttribute ((GdomeElement *)roe->node, aname, value, &exc);

	gdome_str_unref (aname);
	gdome_str_unref (value);
	g_free (s);
}

static RenderObj *
svg_render_path (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node)
{
  RenderContextSVG *ctx_svg = (RenderContextSVG *)ctx;
  SVGContext *svg = ctx_svg->svg;
  RenderObjEditable *result = g_new0 (RenderObjEditable, 1);
  GnomeCanvasBpathDef *bpath;
  GdomeException exc = 0;
  GnomeCanvasItem *item;
  GdkColor color;
  GdomeEventListenerPath *listener;
  GdomeDOMString *tmp;


  bpath = svg_parse_path (node);
  svg_style_push (ctx_svg, node);

  set_gdk_color_from_rgb (&color, svg->fill_color >> 8);

  item = svg_render_bpath (ctx, parent, bpath);
  svg_transform_set (node, item);

  gnome_canvas_bpath_def_unref (bpath);

  /* add listener for updates to path data */
  listener = g_new (GdomeEventListenerPath, 1);
  listener->l.vtab = &svg_path_listener_vtab;
  listener->item = item;
  gdome_n_addEventListener (node,
			    tmp = svg_mk_const_gdome_str ("attrModified"),
			    (GdomeEventListener *)listener, FALSE, &exc);
  gdome_str_unref (tmp);

  result->ro.item = item;
  result->node = node;
  result->move = svg_path_move;
  canvas_make_editable (result);
  svg_style_pop (ctx_svg);

  return (RenderObj *)result;
}

SVGContext *
svg_context_new (void)
{
  SVGContext *svg;

  svg = g_new (SVGContext, 1);
  svg->pixels_per_inch = 96;

  svg->opacity = 0xff;
  svg->fill = 0;
  svg->fill_color = 0;
  svg->fill_opacity = 0xff;
  svg->stroke = 0;
  svg->stroke_color = 0;
  svg->stroke_opacity = 0xff;
  svg->stroke_width = 1;
  svg->font_size = 36;
  svg->cap = GDK_CAP_BUTT;
  svg->join = GDK_JOIN_MITER;
  svg->font_family = "courier";

  return svg;
}

RenderContext *
svg_render_context_new (void)
{
  RenderContextSVG *result;
  GHashTable *dispatch;

  result = g_new (RenderContextSVG, 1);

  result->svg = svg_context_new();
  result->context_stack = g_slist_prepend (NULL, result->svg);
  result->defs = g_hash_table_new (g_str_hash, g_str_equal);

  dispatch = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (dispatch, "svg",        svg_render_svg);
  g_hash_table_insert (dispatch, "g",          svg_render_g);
  g_hash_table_insert (dispatch, "polyline",   svg_render_polyline);
  g_hash_table_insert (dispatch, "polygon",    svg_render_polygon);
  g_hash_table_insert (dispatch, "circle",     svg_render_circle);
  g_hash_table_insert (dispatch, "rect",       svg_render_rect);
  g_hash_table_insert (dispatch, "line",       svg_render_line);
  g_hash_table_insert (dispatch, "ellipse",    svg_render_ellipse);
  g_hash_table_insert (dispatch, "path",       svg_render_path);
  g_hash_table_insert (dispatch, "image",      svg_render_image);
  g_hash_table_insert (dispatch, "text",       svg_render_text);
  g_hash_table_insert (dispatch, "defs",       svg_render_defs);
  g_hash_table_insert (dispatch, "use",        svg_render_use);

  result->rc.dispatch = dispatch;
  return (RenderContext *) result;
}

