/* svg.h

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/


/* This is generic render context stuff for DOMination, strictly a
   prototype. 
*/
/* Note from Mathieu Lacage, 29-12-1999.
   this prototype is expected to stay in gill 
   as part of gill and will probably not evolve in
   an effective implementation of DOMination unless
   someone commits to do it.
*/

#ifndef SVG_CORE_H
#define SVG_CORE_H

typedef struct _RenderContext RenderContext;
typedef struct _RenderObj RenderObj;
typedef struct _RenderObjEditable RenderObjEditable;

typedef RenderObj *RenderFunc (RenderContext *ctx, GnomeCanvasGroup *parent, GdomeNode *node);

struct _RenderContext {
  GHashTable *dispatch; /* maps tagname to render_func */
};

struct _RenderObj {
  GnomeCanvasItem *item;
};

struct _RenderObjEditable {
	RenderObj  ro;
	GdomeNode *node;
	double     button_x, button_y;
	gboolean   button_pressed;
	void       (*move)(RenderObjEditable *roe, double dx, double dy);
};

/* A context specific to SVG rendering */

typedef struct _SVGContext SVGContext;

struct _SVGContext {
  double pixels_per_inch;

  gint opacity; /* 0..255 */

  gboolean fill;
  guint32 fill_color; /* rgb */
  gint fill_opacity; /* 0...255 */

  gboolean stroke;
  guint32 stroke_color; /* rgb */
  gint stroke_opacity; /* 0..255 */
  double stroke_width;

  double font_size;
  
  char *font_family;

  GdkCapStyle cap;
  GdkJoinStyle join;
};

/* This is a subclass of RenderContext, specialized for SVG.

   This single-inheritance mechanism is inadequate for multi-namespace
   tags. But it will do for now.
*/

typedef struct _RenderContextSVG RenderContextSVG;

struct _RenderContextSVG {
  RenderContext rc;
  SVGContext *svg;
  GSList *context_stack;
  GHashTable *defs;
};

RenderObj *domination_render (RenderContext *ctx, GnomeCanvasGroup *parent,
			      GdomeNode *node);

RenderContext *svg_render_context_new (void);

void set_docbase (char *path);
char *get_docbase (void);


#endif /* SVG_CORE_H */
