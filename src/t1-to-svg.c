/* t1-to-svg.c

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "libart_lgpl/art_bpath.h"
#include "libgnomeprint/gt1-parset1.h"


void     out_xy           (FILE *out, double x, double y, double affine[6]);

void     output_as_svg    (ArtBpath *bpath, double affine[6], FILE *out);



#define EPSILON 1e-6
static int
art_ftoa (char str[80], double x)
{
  char *p = str;
  int i, j;

  p = str;
  if (fabs (x) < EPSILON / 2)
    {
      strcpy (str, "0");
      return 1;
    }
  if (x < 0)
    {
      *p++ = '-';
      x = -x;
    }
  if ((int)floor ((x + EPSILON / 2) < 1))
    {
      *p++ = '0';
      *p++ = '.';
      i = sprintf (p, "%06d", (int)floor ((x + EPSILON / 2) * 1e6));
      while (i && p[i - 1] == '0')
	i--;
      if (i == 0)
	i--;
      p += i;
    }
  else if (x < 1e6)
    {
      i = sprintf (p, "%d", (int)floor (x + EPSILON / 2));
      p += i;
      if (i < 6)
	{
	  int ix;

	  *p++ = '.';
	  x -= floor (x + EPSILON / 2);
	  for (j = i; j < 6; j++)
	    x *= 10;
	  ix = floor (x + 0.5);

	  for (j = 0; j < i; j++)
	    ix *= 10;

	  /* A cheap hack, this routine can round wrong for fractions
	     near one. */
	  if (ix == 1000000)
	    ix = 999999;

	  sprintf (p, "%06d", ix);
	  i = 6 - i;
	  while (i && p[i - 1] == '0')
	    i--;
	  if (i == 0)
	    i--;
	  p += i;
	}
    }
  else
    p += sprintf (p, "%g", x);

  *p = '\0';
  return p - str;
}

void
out_xy (FILE *out, double x, double y, double affine[6])
{
  double x1, y1;
  char s1[32], s2[32];

  x1 = x * affine[0] + y * affine[2] + affine[4];
  y1 = x * affine[1] + y * affine[3] + affine[5];
  art_ftoa (s1, x1);
  art_ftoa (s2, y1);
  fprintf (out, "%s %s", s1, s2);
}

void
output_as_svg (ArtBpath *bpath, double affine[6], FILE *out)
{
  int i;
  int closed = 0;

  fprintf (out, "<svg>\n");
  fprintf (out, " <g style=\"fill:#aaf; stroke:#000; stroke-width:2\">\n");
  fprintf (out, "  <path d=\"");
  for (i = 0; bpath[i].code != ART_END; i++)
    {
      switch (bpath[i].code)
	{
	case ART_MOVETO:
	case ART_MOVETO_OPEN:
	  if (closed)
	    fprintf (out, "z");
	  closed = (bpath[i].code == ART_MOVETO);
	  fprintf (out, "M");
	  out_xy (out, bpath[i].x3, bpath[i].y3, affine);
	  break;
	case ART_LINETO:
	  fprintf (out, "L");
	  out_xy (out, bpath[i].x3, bpath[i].y3, affine);
	  break;
	case ART_CURVETO:
	  fprintf (out, "C");
	  out_xy (out, bpath[i].x1, bpath[i].y1, affine);
	  fprintf (out, " ");
	  out_xy (out, bpath[i].x2, bpath[i].y2, affine);
	  fprintf (out, " ");
	  out_xy (out, bpath[i].x3, bpath[i].y3, affine);
	  break;
	default:
	}
    }
  if (closed)
    fprintf (out, "z");
  fprintf (out, "\"/>\n");
  fprintf (out, " </g>\n");
  fprintf (out, "</svg>\n");
}

int
main (int argc, char **argv)
{
  char *fontname;
  char *glyphname;
  int glyphnum;
  Gt1LoadedFont *lf;
  ArtBpath *bpath;
  double wx;
  double affine[6] = {0.5, 0, 0, -0.5, 0, 375};

  if (argc < 3)
    {
      fprintf (stderr, "usage: t1-to-svg font.pfa glyphnum\n");
      exit (1);
    }
  fontname = argv[1];
  glyphname = argv[2];

  lf = gt1_load_font (fontname);
  if (lf == NULL)
    {
      fprintf (stderr, "error loading font %s\n", fontname);
      exit (1);
    }

  glyphnum = atof (glyphname);

  bpath = gt1_get_glyph_outline (lf, glyphnum, &wx);
  if (bpath == NULL)
    {
      fprintf (stderr, "error loading glyph %d\n", glyphnum);
      exit (1);
    }

  output_as_svg (bpath, affine, stdout);

  return 0;
}
