/* testtransform.c

   Copyright (C) 1999 Raph Levien <raph@acm.org>

   Gill is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   Gill is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with Gill; see the file COPYING. If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Raph Levien <raph@acm.org>
*/

#include <gnome.h>
#include <libgdome/gdome.h>
#include "svg.h"

/*

This is a very simple testbed for the transform parser. Please rerun
these tests if you change the parser in any way.


Compile as:

gcc `gnome-config --cflags gnomeui gdome` -c testtransform.c -o testtransform.o

gcc `gnome-config --libs gnomeui gdome gdk_pixbuf` testtransform.o svg.o svg-util.o gnome-canvas-bpath-util.o svg-path.o gt1-parset1.o gt1-dict.o gt1-region.o gt1-namecontext.o gnome-canvas-hacktext.o gnome-canvas-bpath.o svg-path-encode.o parseAFM.o -o testtransform

Then: ./testtransform 'translate(-10,-20) scale(2) rotate(45) translate(5,10)'
should print: [ 1.414214 1.414214 -1.414214 1.414214 -17.071068 1.213203 ]

 */

gboolean
svg_parse_transform (double dst[6], const char *src);

int
main (int argc, char **argv)
{
  double affine[6];

  if (argc == 2 && svg_parse_transform (affine, argv[1]))
    g_print ("[ %f %f %f %f %f %f ]\n",
	     affine[0], affine[1], affine[2], affine[3],
	     affine[4], affine[5]);
  return 0;
}
